**Budowanie projektu**

do zbudowania projektu niezbędne jest JDK11+

```sh
$ ./gradlew bootJar
```

zbudowany jar będzie się znajdował w build/lib/web-security-learning-platform-backend-0.0.1-SNAPSHOT.jar

**Odpalenie aplikacji**

```sh
$ java -jar web-security-learning-platform-backend-0.0.1-SNAPSHOT.jar
```

**Budowa obrazu dockerowego**

```sh
$ docker build --tag web-security-training-platform .
```
**Uruchomienie obrazu**
```sh
$ docker run --publish 8080:8080 --name training web-security-training-platform
```
lub
```sh
$ cd docker
$ docker-compose up
```
