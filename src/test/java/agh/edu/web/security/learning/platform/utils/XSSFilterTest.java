package agh.edu.web.security.learning.platform.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XSSFilterTest {

    @Test
    void shouldBlockSimpleJSScript() {
        //given
        String script = "<script>alert(1)</script>";
        //when
        String result = XSSFilter.filterXSS1(script);
        //then
        assertEquals("error", result);
    }

    @Test
    void shouldNotBlockSimpleJSScript() {
        //given
        String script = "<script>\nalert(1)</script>";
        //when
        String result = XSSFilter.filterXSS1(script);
        //then
        assertEquals(script, result);
    }

    @Test
    void shouldRemoveParenthesisFromScript() {
        //given
        String script = "<script>alert(1)</script>";
        //when
        String result = XSSFilter.filterXSS2(script);
        //then
        assertEquals("<script>alert1</script>", result);
    }
}