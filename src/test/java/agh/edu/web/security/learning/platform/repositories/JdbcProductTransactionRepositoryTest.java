package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductTransaction;
import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductTransactionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@ActiveProfiles("test")
class JdbcProductTransactionRepositoryTest {

    @Autowired
    ProductTransactionRepository productTransactionRepository;

    @Test
    void shouldReturnProductsForUser(){
        //given
        String id = "1";
        //when
        List<Product> productList = productTransactionRepository.getUserProducts(id);
        //then
        assertFalse(productList.isEmpty());
    }

    @Test
    void shouldReturnUsersForProduct(){
        //given
        String id = "1";
        //when
        List<User> userList = productTransactionRepository.getProductBuyers(id);
        //then
        assertFalse(userList.isEmpty());
    }

    @Test
    void shouldReturnTransactionForUser(){
        //given
        String id = "1";
        Long longId = 1L;
        //when
        List<ProductTransaction> productTransactionList = productTransactionRepository.getTransactionByUserId(id);
        //then
        assertFalse(productTransactionList.isEmpty());
        productTransactionList.forEach(e -> assertEquals(longId, e.getUserId()));
    }

    @Test
    void shouldReturnProductTransactionForProduct(){
        //given
        String id = "1";
        Long longId = 1L;
        //when
        List<ProductTransaction> productTransactionList = productTransactionRepository.getTransactionByProductId(id);
        //then
        assertFalse(productTransactionList.isEmpty());
        productTransactionList.forEach(e -> assertEquals(longId, e.getProductId()));
    }

    @Test
    void shouldReturnProductTransactionForAllUsers(){
        //given
        String id = "1 OR 1=1";
        Long longId = 1L;
        //when
        List<ProductTransaction> productTransactionList = productTransactionRepository.getTransactionByUserId(id);
        //then
        assertFalse(productTransactionList.isEmpty());
        Optional<ProductTransaction> result = productTransactionList.stream().filter(e -> !e.getUserId().equals(longId)).findAny();
        assertTrue(result.isPresent());
    }

    @Test
    void shouldReturnProductTransactionForAllProducts(){
        //given
        String id = "1 OR 1=1";
        Long longId = 1L;
        //when
        List<ProductTransaction> productTransactionList = productTransactionRepository.getTransactionByProductId(id);
        //then
        assertFalse(productTransactionList.isEmpty());
        Optional<ProductTransaction> result = productTransactionList.stream().filter(e -> !e.getProductId().equals(longId)).findAny();
        assertTrue(result.isPresent());
    }
}