package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductTransaction;
import agh.edu.web.security.learning.platform.models.ProductType;
import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductRepository;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductTransactionRepository;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class JdbcProductRepositoryTest {

    private static final Logger log = Logger.getLogger(JdbcProductRepositoryTest.class.getName());

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductTransactionRepository productTransactionRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void shouldReturnProduct() {
        //given
        String id = "1";
        //when
        Optional<Product> product = productRepository.getProductById(id);
        //then
        assertTrue(product.isPresent());
    }

    @Test
    void shouldReturnUserProducts() {
        //given
        String id = "1";
        //when
        List<Product> products = productRepository.getAllUserProducts(id);
        //then
        assertFalse(products.isEmpty());
    }


    @Test
    void shouldReturnAllUserProducts() {
        //given
        String id = "1 OR 1=1";
        Long longId = 1L;
        String otherId = "2";
        //when
        List<Product> products = productRepository.getAllUserProducts(id);
        List<ProductTransaction> productTransactionList = productTransactionRepository.getTransactionByUserId(otherId);
        assertFalse(productTransactionList.isEmpty());
        ProductTransaction exampleProductTransaction = productTransactionList.get(0);
        //then
        assertFalse(products.isEmpty());
        Optional<Product> product = products.stream().filter(e -> !e.getId().equals(exampleProductTransaction.getProductId())).findAny();
        assertTrue(product.isPresent());
    }

    @Test
    void shouldBeAbleToExecUnionAttack() {
        //given
        String id = "1 OR 1=1 UNION SELECT id, name, '19.22', password, 'BOOK' from users u";
        String userId = "1";
        //when
        List<Product> products = productRepository.getAllUserProducts(id);
        Optional<User> user = userRepository.findUserById(userId);
        //then
        assertFalse(products.isEmpty());
        assertTrue(user.isPresent());
        Optional<Product> result = products.stream().filter(e -> e.getName().equals(user.get().getName())).findAny();
        assertTrue(result.isPresent());
    }

    @Test
    void shouldNotBeAbleToExecuteUnionAttack() {
        //given
        String id = "1 OR 1=1 UNION SELECT id, name, '19.22', password, 'BOOK' from users u";
        //when
        List<Product> products = productRepository.getAllUserProductsWithoutUnion(id);
        //then
        assertTrue(products.isEmpty());
    }

    @Test
    void shouldReturnAllUserProductsByName() {
        //given
        String userId = "1";
        String productName = "prod";
        //when
        List<Product> products = productRepository.getAllProductByNameForUser(userId, productName);
        //then
        assertFalse(products.isEmpty());
    }

    @Test
    void shouldReturnAllUSerProductsByType() {
        //given
        String userId = "1";
        ProductType type = ProductType.BOOK;
        //when
        List<Product> products = productRepository.getAllUserProductByType(userId, type.name());
        //then
        assertFalse(products.isEmpty());
        products.forEach(e -> assertEquals(e.getType(),type));
        products.forEach(e -> assertNotEquals(e.getType(), ProductType.ELECTRONIC));
    }

    @Test
    void shouldReturnAllProductsByTypeSQLInjection() {
        //given
        String userId = "1";
        ProductType type = ProductType.BOOK;
        //when
        String malformedQuery = type.name() + "' OR '1'='1";
        List<Product> productsInjected = productRepository.getAllUserProductByType(userId, malformedQuery);
        List<Product> products = productRepository.getAllUserProductByType(userId, type.name());
        //then
        assertFalse(products.isEmpty());
        assertFalse(productsInjected.isEmpty());
//        log.info("ProductsInjected: " + productsInjected.stream().map(Object::toString).reduce("", (e1, e2) -> e1 + ", " + e2));
//        log.info("Products: " + products.stream().map(Object::toString).reduce("", (e1, e2) -> e1 + ", " + e2));
        assertTrue(productsInjected.containsAll(products));
        assertTrue(products.size() < productsInjected.size());
    }

    @Test
    void shouldNotProcessQueryWithSpace() {
        //given
        String userId = "1";
        String name = "%' OR '1%'='1";
        //when
        List<Product> products = productRepository.getAllProductByNameForUserWithFilter(userId, name);
        //then
        assertTrue(products.isEmpty());
    }

    @Test
    void shouldNotProcessQueryWitSpace() {
        //given
        String userId = "1";
        String name = "%'OR'1%'='1";
        //when
        List<Product> products = productRepository.getAllProductByNameForUserWithFilter(userId, name);
        //then
        assertFalse(products.isEmpty());
    }

    @Test
    void shouldReturnSortedProductsByDescription() {
        //given
        String userId = "1";
        String column = "2";
        //when
        List<Product> products = productRepository.getAllUserProductSortedByColumn(userId, column);
        List<Product> sortedProducts = new ArrayList<>(products);
        sortedProducts.sort(Comparator.comparing(Product::getName));
        //then
        assertEquals(sortedProducts, products);
    }

    @Test
    void shouldReturnSortedListInOtherWay() {
        //given
        String userId = "1";
        String columnDesc = "2 DESC";
        String columnAsc = "(CASE WHEN (SELECT ASCII(SUBSTRING(email,1,1)) FROM users WHERE ID = '1')=97 THEN 1 ELSE 2)";
        //when
        List<Product> productsAsc = productRepository.getAllUserProductSortedByColumn(userId, columnAsc);
        List<Product> productsDesc = productRepository.getAllUserProductSortedByColumn(userId, columnDesc);
        //then
        assertFalse(productsAsc.isEmpty());
        assertFalse(productsDesc.isEmpty());
        assertTrue(productsAsc.containsAll(productsDesc));
        assertEquals(productsDesc.size(), productsAsc.size());
        for (int i = 0; i < productsDesc.size(); i++) {
            assertEquals(productsDesc.get(i).getName(), productsAsc.get(productsAsc.size()-i-1).getName());
        }
    }

//    @Test
//    void shouldProcessMappedParameters() {
//        //given
//        String name = "cake";
//        String price = "22.22";
//        String description = "some 1231231231";
//        String category = "FOOD";
//        //when
//        List<Product> products = productRepository.getAllProductOffersWithMappedParameters(name, price, description, category, "1");
//        //then
//        assertFalse(products.isEmpty());
//        Product product = products.get(0);
//        assertEquals(product.getName(), name);
//        assertEquals(product.getDescription(), Optional.of(description));
//        assertEquals(product.getType(), ProductType.FOOD);
//        assertEquals(product.getPrice(), price);
//    }
//
//    @Test
//    void shouldBeAbleTOInjectMappedParameters() {
//        //given
//        String name = "cake";
//        String price = "22.22";
//        String description = "some 1231231231";
//        String category = "FOOD' OR '1'='1";
//        //when
//        List<Product> products = productRepository.getAllProductOffersWithMappedParameters(name, price, description, category, "1");
//        //then
//        assertFalse(products.isEmpty());
//        assertTrue(products.size() > 1);
//    }

    @Test
    void shouldAddProductToRepository() {
        //given
        Product product = new Product(1L, "ss", "12.11", "description", ProductType.BOOK);
        //when
        int result = productRepository.saveProduct(product);
        //then
        assertTrue(result > 0);
    }

    @Test
    void shouldNotAddProductToRepository() {
        //given
        Product product = new Product(1L, null, "12.11", "description", ProductType.BOOK);
        //when
        int result = productRepository.saveProduct(product);
        //then
        assertEquals(-1, result);
    }
}