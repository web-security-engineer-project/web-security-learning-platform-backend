package agh.edu.web.security.learning.platform.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class BrokenAuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnMainAuth() throws Exception {
        mockMvc.perform(get("/auth"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth"));
    }

    @Test
    void shouldReturnExercise1Page() throws Exception {
        mockMvc.perform(get("/auth/zad1"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad1"));
    }

    @Test
    void shouldNotAuthorizeUserExercise1() throws Exception {
        mockMvc.perform(post("/auth/zad1/authorize")
                        .param("login", "dev")
                        .param("password", "zzzz"))
                .andExpect(status().isUnauthorized())
                .andExpect(view().name("src/auth/zad1"));
    }

    @Test
    void shouldAuthorizeUserExercise1() throws Exception {
        mockMvc.perform(post("/auth/zad1/authorize")
                .param("login", "dev")
                .param("password", "n^4RSp(N5UdTzA5X"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/result/success"));
    }

    @Test
    void shouldNotAcceptLoginAttemptExercise1() throws Exception {
        mockMvc.perform(post("/auth/zad1/authorize").content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnExercise2Page() throws Exception {
        mockMvc.perform(get("/auth/zad2"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad2"));
    }

    @Test
    void shouldAuthorizeUserExercise2() throws Exception {
        mockMvc.perform(post("/auth/zad2/authorize")
                .param("login", "admin")
                .param("password", "ht4CA\\jY]A{k\"j]D"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/result/success"));
    }

    @Test
    void shouldNotAuthorizeUserExercise2() throws Exception {
        mockMvc.perform(post("/auth/zad2/authorize")
                .param("login", "admin")
                .param("password", "asdasdasdasd"))
                .andExpect(status().isUnauthorized())
                .andExpect(view().name("src/auth/zad2"));
    }


    @Test
    void shouldNotAcceptLoginAttemptExercise2() throws Exception {
        mockMvc.perform(post("/auth/zad2/authorize").content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldNotAcceptLoginAttemptExercise3() throws Exception {
        mockMvc.perform(post("/auth/zad3/authorize").content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldNotAuthorizeUserExercise3() throws Exception {
        mockMvc.perform(post("/auth/zad3/authorize")
                .param("login", "admin")
                .param("password", "asdasdasdasd"))
                .andExpect(status().isUnauthorized())
                .andExpect(view().name("src/auth/zad3"));
    }

    @Test
    void shouldAuthorizeUserExercise3() throws Exception {
        mockMvc.perform(post("/auth/zad3/authorize")
                .param("login", "some@email.pl' OR '1'='1")
                .param("password", "asdasdasdasd"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/result/success"));
    }

    @Test
    void shouldReturnExercise3Page() throws Exception {
        mockMvc.perform(get("/auth/zad3"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad3"));
    }

    @Test
    void shouldNotAuthorizeUserExercise4() throws Exception {
        mockMvc.perform(post("/auth/zad4/authorize")
                .param("login", "admin")
                .param("password", "asdasdasdasd"))
                .andExpect(status().isUnauthorized())
                .andExpect(view().name("src/auth/zad4"));
    }
    @Test
    void shouldAuthorizeUserExercise4() throws Exception {
        mockMvc.perform(post("/auth/zad4/authorize"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/result/success"));
    }


    @Test
    void shouldReturnExercise4Page() throws Exception {
        mockMvc.perform(get("/auth/zad4"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad4"));
    }

    @Test
    void shouldReturnExercise5Page() throws Exception {
        mockMvc.perform(get("/auth/zad5"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad5"));
    }

    @Test
    void shouldReturnExercise6Page() throws Exception {
        mockMvc.perform(get("/auth/zad6"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad6"));
    }

    @Test
    void shouldReturnExercise7Page() throws Exception {
        mockMvc.perform(get("/auth/zad7"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/zad7"));
    }

    @Test
    void shouldNotAuthorizeUserExercise5() throws Exception {
        mockMvc.perform(post("/auth/zad5/authorize").param("login", "login").param("password", "password"))
                .andExpect(status().isUnauthorized())
                .andExpect(view().name("src/auth/zad5"));
    }

    @Test
    void shouldAuthorizeUserExercise5() throws Exception {
        mockMvc.perform((post("/auth/zad5/authorize").param("login", "woo").param("password", "woo")))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/result/success"));
    }

    @Test
    void shouldReturnForgotPage() throws Exception {
        mockMvc.perform(get("/auth/password-recovery-email"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/password-recovery-email"));
    }

    @Test
    void shouldProcessToPasswordRecoveryQuestion() throws Exception {
        mockMvc.perform(post("/auth/zad6/password-question").param("login", "mark@mail.com"))
                .andExpect(status().isOk())
                .andExpect(cookie().exists("security-cookie"))
                .andExpect(view().name("src/auth/password-recovery"));
    }

    @Test
    void shouldNotProcessToPasswordRecoveryQuestion() throws Exception {
        mockMvc.perform(post("/auth/zad6/password-question").param("login", "mark@zzzzl.com"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("src/auth/password-recovery-email"))
                .andExpect(model().attributeExists("notfound"));
    }

    @Test
    void shouldAuthenticateUserExercise6() throws Exception {
        mockMvc.perform(post("/auth/zad6/password-recovery-question")
                    .param("color", "White")
                    .cookie(new Cookie("security-cookie", "Nfa`$x4aE3k7qqS#>!w8Z>`[<")))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/password-recovery"))
                .andExpect(model().attributeExists("password"));
    }

    @Test
    void shouldNotAuthenticateUserExercise6() throws Exception {
        mockMvc.perform(post("/auth/zad6/password-recovery-question")
                .param("color", "Black")
                .cookie(new Cookie("security-cookie", "Nfa`$x4aE3k7qqS#>!w8Z>`[<")))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/password-recovery"))
                .andExpect(model().attributeExists("error"));
    }

    @Test
    void shouldNotAccessSecurityQuestionWithoutValidCookieUserExercise6() throws Exception {
        mockMvc.perform(post("/auth/zad6/password-recovery-question")
                .param("color", "Black")
                .cookie(new Cookie("security-cookie", "Nfa`$x4>!w8Z>`[<")))
                .andExpect(status().is(401))
                .andExpect(view().name("src/auth/zad6"));
    }


    @Test
    void shouldNotAccessSecurityQuestionWithoutCookieUserExercise6() throws Exception {
        mockMvc.perform(post("/auth/zad6/password-recovery-question")
                .param("color", "Black"))
                .andExpect(status().is(400));
    }

    @Test
    void shouldAuthorizeUserExercise7() throws Exception {
        mockMvc.perform((post("/auth/zad7/authorization").param("login", "youshould").param("password", "notusebasicauth")))
                .andExpect(status().isOk())
                .andExpect(view().name("src/auth/result/success"));
    }

    @Test
    void shouldNotAuthorizeUserExercise7() throws Exception {
        mockMvc.perform((post("/auth/zad7/authorization").param("login", "youshould").param("password", "usebasicauth")))
                .andExpect(status().isUnauthorized())
                .andExpect(view().name("src/auth/zad7"))
                .andExpect(model().attributeExists("error"));
    }
}