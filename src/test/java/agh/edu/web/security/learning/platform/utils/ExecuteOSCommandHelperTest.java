package agh.edu.web.security.learning.platform.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class ExecuteOSCommandHelperTest {

    @Test
    void osExecuteDnsLookupGoogleWindows() {
        assumeTrue(System.getProperty("os.name").contains("Windows"));
        //given
        String ip = "8.8.8.8";
        //when
        String result = ExecuteOSCommandHelper.executeCommand("nslookup ", ip);
        //then
        assertNotNull(result);
        assertTrue(result.contains("dns.google"));
    }

    @Test
    void osExecutePingCommandWindows() {
        assumeTrue(System.getProperty("os.name").contains("Windows"));
        //given
        String ip = "8.8.8.8";
        //when
        String result = ExecuteOSCommandHelper.executeCommand("ping ", ip);
        //then
        assertNotNull(result);
        assertTrue(result.contains("Reply from " + ip));
    }


    @Test
    void osExecuteOsInjectionWindows() {
        assumeTrue(System.getProperty("os.name").contains("Windows"));
        //given
        String ip = "8.8.8.8 & echo \"whooops\"";
        //when
        String result = ExecuteOSCommandHelper.executeCommand("nslookup ", ip);
        //then
        assertNotNull(result);
        assertTrue(result.contains("dns.google"));
        assertTrue(result.contains("whooops"));
    }

    @Test
    void osExecuteNsLookupGoogleBash() {
        String os = System.getProperty("os.name");
        assumeTrue(os.toLowerCase().contains("nux") || os.toLowerCase().contains("mac"));
        //given
        String ip = "8.8.8.8";
        //when
        String result = ExecuteOSCommandHelper.executeCommand("nslookup ", ip);
        //then
        assertNotNull(result);
        assertTrue(result.contains("dns.google"));
    }

    @Test
    void osExecuteOSInjectionBash() {
        String os = System.getProperty("os.name");
        assumeTrue(os.toLowerCase().contains("nux") || os.toLowerCase().contains("mac"));
        //given
        String ip = "8.8.8.8 & echo \"whooops\"";
        //when
        String result = ExecuteOSCommandHelper.executeCommand("nslookup ", ip);
        //then
        assertNotNull(result);
        assertTrue(result.contains("dns.google"));
        assertTrue(result.contains("whoops"));
    }
}