package agh.edu.web.security.learning.platform.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class OSInjectionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnSimpleOSInjectionPageGET() throws Exception {
        mockMvc.perform(get("/injection/os/simple"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/os/simple"));
    }

    @Test
    void shouldReturnSimpleOSInjectionPagePOST() throws Exception {
        mockMvc.perform(post("/injection/os/simple").param("ip", "param"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/os/simple"))
                .andExpect(model().attributeExists("output"));
    }

    @Test
    void shouldReturnBlindOSInjectionPageGET() throws Exception {
        mockMvc.perform(get("/injection/os/blind"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/os/blind"));
    }

    @Test
    void shouldReturnBlindOSInjectionPagePOST() throws Exception {
        mockMvc.perform(post("/injection/os/blind").param("ip", "param"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/os/blind"))
                .andExpect(model().attributeExists("output"));
    }
}