package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.Application;
import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductRepository;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class InjectionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnValidPage() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void shouldReturnInjectionMainPage() throws Exception {
        mockMvc.perform(get("/injection"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection"));
    }

    @Test
    void shouldReturnPageExerciseOneWithoutParameter() throws Exception {
        mockMvc.perform(get("/injection/zad1"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad1"));
    }

    @Test
    void shouldReturnPageExerciseOneWithParameter() throws Exception {
        mockMvc.perform(get("/injection/zad1").param("productName", "value"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad1"));
    }


    @Test
    void shouldReturnPageExerciseTwoGet() throws Exception {
        mockMvc.perform(get("/injection/zad2"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad2"));
    }

    @Test
    void shouldReturnPageExerciseTwoPost() throws Exception {
        mockMvc.perform(post("/injection/zad2").param("type", "something"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad2"));
    }

    @Test
    void shouldReturnExerciseFourPageGet() throws Exception {
        mockMvc.perform(get("/injection/zad4"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad4"));
    }
    @Test
    void shouldReturnExerciseFourPagePost() throws Exception {
        mockMvc.perform(post("/injection/zad4").param("productName", "smth"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad4"));
    }

    @Test
    void shouldReturnExerciseFivePagePost() throws Exception {
        mockMvc.perform(get("/injection/zad5"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad5"));
    }

    @Test
    void shouldReturnExerciseSixPageGet() throws Exception {
        mockMvc.perform(get("/injection/zad6"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad6"));
    }

    @Test
    void shouldAcceptAllParameters() throws Exception {
        mockMvc.perform(get("/injection/zad7")
                            .param("product[name]", "name")
                            .param("product[price]", "price")
                            .param("product[description]", "description")
                            .param("product[category]", "category"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad7"))
                .andExpect(model().attributeExists("products"));
    }

    @Test
    void shouldReturnPageToExercise8GET() throws Exception {
        mockMvc.perform(get("/injection/zad8"))
                .andExpect(status().isOk())
                .andExpect(view().name("src/injection/zad8"))
                .andExpect(model().attributeExists("user"));
    }
}