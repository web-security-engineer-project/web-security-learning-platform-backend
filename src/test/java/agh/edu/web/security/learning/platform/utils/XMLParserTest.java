package agh.edu.web.security.learning.platform.utils;

import agh.edu.web.security.learning.platform.models.Comment;
import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductType;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class XMLParserTest {

    private XMLParser parser = new XMLParser();

    @Test
    public void parseSimpleComment() throws JAXBException, XMLStreamException {
        //given
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<comment>\n" +
                "<text>wiadomosc</text>\n" +
                "<user>user</user>\n" +
                "</comment>";
        Comment comment = new Comment("wiadomosc", "user");
        //when
        Comment parsedComment = parser.parseXml(xml);
        //then
        assertEquals(comment, parsedComment);
    }


    @Test
    void shouldBeVulnerableToXXEInjection() throws JAXBException, XMLStreamException {
        //given
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE note [\n" +
                "<!ENTITY writer \"Writer: Donald Duck.\">\n" +
                "<!ENTITY copyright \"Copyright: Maciej W\">\n" +
                "]>" +
                "<comment>\n" +
                "<text>&writer;&copyright;</text>\n" +
                "<user>user</user>\n" +
                "</comment>";
        Comment comment = new Comment("Writer: Donald Duck.Copyright: Maciej W", "user");
        //when
        Comment parsedComment = parser.parseXml(xml);
        //then
        assertEquals(comment, parsedComment);
    }

    @Test
    void shouldParseSimpleProduct() throws JAXBException, XMLStreamException {
        //given
        Product product = new Product(1L,"text", "text", "text", ProductType.BOOK);
        String xml =
                "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "      <product>\n" +
                "        <id>1</id>\n" +
                "        <name>text</name>\n" +
                "        <price>text</price>\n" +
                "        <description>text</description>\n" +
                "        <type>BOOK</type>\n" +
                "      </product> ";
        //when
        Product parsedProduct = parser.parseProduct(xml);
        parsedProduct.setDescription("text");
        //then
        assertEquals(product, parsedProduct);
    }

    @Test
    void shouldBeVulnerableToXXEInjectionProduct() throws JAXBException, XMLStreamException {
        //given
        Product product = new Product(1L,"Writer: Donald Duck.", "text", "text", ProductType.BOOK);
        String xml =
                "<?xml version='1.0' encoding='UTF-8'?>\n" +
                        "<!DOCTYPE note [\n" +
                        "<!ENTITY writer \"Writer: Donald Duck.\">\n" +
                        "<!ENTITY copyright \"Copyright: Maciej W\">\n" +
                        "]>" +
                        "      <product>\n" +
                        "        <id>1</id>\n" +
                        "        <name>&writer;</name>\n" +
                        "        <price>text</price>\n" +
                        "        <description>text</description>\n" +
                        "        <type>BOOK</type>\n" +
                        "      </product> ";
        //when
        Product parsedProduct = parser.parseProduct(xml);
        parsedProduct.setDescription("text");
        //then
        assertEquals(product, parsedProduct);
    }
}