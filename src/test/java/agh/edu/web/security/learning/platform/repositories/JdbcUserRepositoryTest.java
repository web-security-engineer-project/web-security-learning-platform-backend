package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class JdbcUserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void shouldAuthorizeUser() {
        String email = "some@email.pl' OR '1'='1";
        String password = "whatever";
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        assertTrue(userRepository.userAuthorization(user));
    }

    @Test
    void updateUserInfoTest() {
        //given

    }

}