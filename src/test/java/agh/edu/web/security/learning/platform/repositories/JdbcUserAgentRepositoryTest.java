package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.UserAgent;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class JdbcUserAgentRepositoryTest {

    @Autowired
    JdbcUserAgentRepository jdbc;

    @Test
    void shouldReturnUserAgent() {
        //given
        String id = "1";
        //when
        Optional<UserAgent> userAgent = jdbc.findUserUserAgent(id);
        //then
        assertTrue(userAgent.isPresent());
    }

    @Test
    void shouldUpdateUserAgent() {
        //given
        String id = "1";
        String userAgentValue = "Firefox";
        //when
        jdbc.saveUserAgent(id, userAgentValue);
        Optional<UserAgent> userAgent = jdbc.findUserUserAgent(id);
        //then
        assertTrue(userAgent.isPresent());
        assertEquals(userAgentValue, userAgent.get().getUserAgentName());

    }

    /*
    Test need to be ignore, testing if droping database is possible
     */
    @Disabled
    @Test
    void shouldInjectDeletingAllRows() {
        //given
        String id = "1";
        String userAgentValue = "Firefox'; delete user_agent where 1=1;--";
        //when
        jdbc.saveUserAgent(id, userAgentValue);
        Optional<UserAgent> userAgent = jdbc.findUserUserAgent(id);
        //then
        assertTrue(userAgent.isEmpty());
    }
}