package agh.edu.web.security.learning.platform.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class JWTControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnPageToExercise1() throws Exception {
        mockMvc.perform(get("/jwt/zad1"))
                .andExpect(view().name("src/auth/jwt/zad1"));
    }

    @Test
    void shouldReturnPageToExercise2() throws Exception {
        mockMvc.perform(get("/jwt/zad2"))
                .andExpect(view().name("src/auth/jwt/zad2"));
    }

    @Test
    void shouldReturnPageToExercise3() throws Exception {
        mockMvc.perform(get("/jwt/zad3"))
                .andExpect(view().name("src/auth/jwt/zad3"));
    }
//
//    @Test
//    void shouldDeleteCommentExercise1() {
//        mockMvc.perform(delete("/jwt/zad1/deleteComment").header("token", "zzz.zzz.zzzz"))
//                .andExpect()
//    }
}