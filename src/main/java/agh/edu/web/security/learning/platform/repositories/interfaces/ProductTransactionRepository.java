package agh.edu.web.security.learning.platform.repositories.interfaces;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductTransaction;
import agh.edu.web.security.learning.platform.models.User;

import java.util.List;

public interface ProductTransactionRepository {
    List<User> getProductBuyers(String id);
    List<Product> getUserProducts(String id);
    List<ProductTransaction> getTransactionByUserId(String id);
    List<ProductTransaction> getTransactionByProductId(String id);
}
