package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.models.UserAgent;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductRepository;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserAgentRepository;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserRepository;
import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.rmi.ServerException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class InjectionController {

    private static final Logger log = Logger.getLogger(InjectionController.class.getName());

    public InjectionController(UserAgentRepository userAgentRepository, UserRepository userRepository, ProductRepository productRepository) {
        this.userAgentRepository = userAgentRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    private UserAgentRepository userAgentRepository;

    private UserRepository userRepository;

    private ProductRepository productRepository;

    private static final String DEFAULT_USER_PRODUCT_ID = "1";

    @GetMapping({"/injection"})
    public String injection(Model model, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        model.addAttribute("user", new User());
        return "src/injection";
    }

    @GetMapping({"/injection/zad1"})
    public String getExercise1(@RequestParam(value = "productName", required = false) String name, Model model, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        List<Product> productList = new ArrayList<>();
        try {
            if (name != null) {
                productList = productRepository.getAllProductByNameForUser(DEFAULT_USER_PRODUCT_ID, name);
            } else {
                productList = productRepository.getAllUserProducts(DEFAULT_USER_PRODUCT_ID);
            }
        } catch (Exception e) {
            e.getStackTrace();
            log.log(Level.WARNING, "Unsuccessful query for injection/zad1\n Cause: " + e.getCause());
        }
        model.addAttribute("products", productList);
        return "src/injection/zad1";
    }

    @GetMapping({"/injection/zad2"})
    public String getExercise2(Model model, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var list = getProductListForUser();
        model.addAttribute("products", list);
        return "src/injection/zad2";
    }

    @PostMapping(value = "/injection/zad2")
    public ModelAndView postExercise2(@RequestParam  Map<String,String> map, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/zad2");
        List<Product> productList = new ArrayList<>();
        try {
            if (map.containsKey("type")) {
                productList = productRepository.getAllUserProductByType(DEFAULT_USER_PRODUCT_ID, map.get("type"));
            } else {
                productList = productRepository.getAllUserProducts(DEFAULT_USER_PRODUCT_ID);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unsuccessful query for injection/zad2\n Cause: " + e.getCause());
        }
        modelAndView.addObject("products", productList);
        return modelAndView;
    }

    @GetMapping({"/injection/zad3"})
    public ModelAndView postExercise3(@RequestParam(value = "type", required = false)  String type, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/zad3");
        List<Product> productList = new ArrayList<>();
        try {
            if (type != null) {
                productList = productRepository.getAllUserProductByType(DEFAULT_USER_PRODUCT_ID, type);
            } else {
                productList = productRepository.getAllUserProducts(DEFAULT_USER_PRODUCT_ID);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unsuccessful query for injection/zad2\n Cause: " + e.getCause());
        }
        modelAndView.addObject("products", productList);
        return modelAndView;
    }

    @GetMapping({"/injection/zad4"})
    public ModelAndView getExercise4(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/zad4");
        var productList = getProductListForUser();
        modelAndView.addObject("products", productList);
        return modelAndView;
    }

    @PostMapping({"/injection/zad4"})
    public ModelAndView processPostExercise4(@RequestParam(value = "productName") String productName, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/zad4");
        List<Product> productList = new ArrayList<>();
        try {
            if (productName != null) {
                productList = productRepository.getAllProductByNameForUserWithFilter(DEFAULT_USER_PRODUCT_ID, productName);
            } else {
                productList = productRepository.getAllUserProducts(DEFAULT_USER_PRODUCT_ID);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unsuccessful query for injection/zad2\n Cause: " + e.getCause());
        }
        modelAndView.addObject("products", productList);
        return modelAndView;
    }

    @GetMapping({"/injection/zad5"})
    public ModelAndView getExercise5(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/zad5");
        var productList = getProductListForUser();
        modelAndView.addObject("products", productList);
        return modelAndView;
    }

    @PostMapping({"/injection/zad5"})
    public ModelAndView processExercise5(@RequestParam("column") String column,  HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/zad5");
        List<Product> productList = new ArrayList<>();
        try {
            if (column != null) {
                productList = productRepository.getAllUserProductSortedByColumn(DEFAULT_USER_PRODUCT_ID, column);
            } else {
                productList = productRepository.getAllUserProducts(DEFAULT_USER_PRODUCT_ID);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Unsuccessful query for injection/zad2\n Cause: " + e.getCause());
        }
        modelAndView.addObject("products", productList);
        return modelAndView;
    }

    @GetMapping({"/injection/zad6"})
    public String getExercise6(@RequestHeader(value = "User-Agent", required = false)String userAgent, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        if (userAgent != null) {
            userAgentRepository.saveUserAgent(DEFAULT_USER_PRODUCT_ID, userAgent);
        }
        return "src/injection/zad6";
    }

    @GetMapping({"/injection/zad7"})
    public ModelAndView getExercise7(@RequestParam(value = "product[name]", required = false) String name,
                                     @RequestParam(value = "product[price]", required = false) String price,
                                     @RequestParam(value = "product[description]", required = false) String description,
                                     @RequestParam(value = "product[category]", required = false) String category,
                                     HttpServletResponse response, HttpServletRequest request) {
        HttpHeaderUtil.addHeaders(response);
        Enumeration<String> names = request.getParameterNames();
        var modelAndView = new ModelAndView("src/injection/zad7");
        List<Product> products = new ArrayList<>();
        try {
            products = productRepository.getAllProductOffersWithMappedParameters(request);
        } catch (Exception e) {
            e.getStackTrace();
        }
        modelAndView.addObject("products", products);
        return modelAndView;
    }

    public List<Product> getProductListForUser() {
        List<Product> productList = new ArrayList<>();
        try {
            productList = productRepository.getAllUserProducts(DEFAULT_USER_PRODUCT_ID);
        } catch (Exception e) {
            log.log(Level.WARNING, "Unsuccessful query for injection/zad4\n Cause: " + e.getMessage());
        }
        return productList;
    }
}
