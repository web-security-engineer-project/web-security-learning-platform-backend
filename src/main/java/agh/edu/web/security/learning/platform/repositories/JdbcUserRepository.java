package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.models.UserAgent;
import agh.edu.web.security.learning.platform.models.mappers.UserAgentMapper;
import agh.edu.web.security.learning.platform.models.mappers.UserMapper;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class JdbcUserRepository implements UserRepository {

    @Autowired
    private JdbcTemplate jdbc;

    public JdbcUserRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public boolean userAuthorization(User user) {
        if (user != null && user.getEmail() != null && user.getPassword() != null) {
            var result = jdbc.queryForObject("SELECT COUNT(*) FROM users u WHERE u.email='" + user.getEmail() + "' AND u.password='" + user.getPassword() + "'"
                    , Integer.class);
            if (result != null && result >= 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Optional<User> findUserById(String id) {
        var unsafeQuery = "SELECT u.* FROM users u WHERE u.id = " + id;
        User user = null;
        try {
            user = jdbc.queryForObject(unsafeQuery, new UserMapper());
        } catch (Exception e) {
            return Optional.empty();
        }
        return Optional.of(user);
    }

    @Override
    public void updateUserInfo(User user) {
        var unsafeQuery = new StringBuilder("UPDATE users SET");
        boolean addComa = false;
        if (user.getEmail() != null) {
            unsafeQuery.append(" email=").append(user.getEmail());
            addComa = true;
        }
        if (user.getName() != null) {
            if (addComa) {
                unsafeQuery.append(", ");
            } else {
                addComa = true;
            }
            unsafeQuery.append(" name=").append(user.getName());
        }

        if (user.getPersonalInformation().isPresent()) {
            if (addComa) {
                unsafeQuery.append(", ");
            } else {
                addComa = true;
            }
            unsafeQuery.append(" personal_information=").append(user.getName());
        }
        unsafeQuery.append(" WHERE id=").append(user.getId());
        jdbc.query(unsafeQuery.toString(), new UserMapper());
    }
}
