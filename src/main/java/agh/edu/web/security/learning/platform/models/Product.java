package agh.edu.web.security.learning.platform.models;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;
import java.util.Optional;

@Entity
@XmlRootElement
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "id")
    private Long id;
    @Column(nullable = false, name = "name")
    private String name;
    @Column(nullable = false, name = "price")
    @Pattern(regexp = "\\d+\\.\\d{2}")
    private String price;
    @Column(nullable = true, name = "description")
    private String description;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ProductType type;
    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", description='" + description + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(description, product.description) &&
                type == product.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, description, type);
    }


    public Product() {
    }

    public Product(Long id, String name, @Pattern(regexp = "\\d+\\.\\d{2}") String price, String description, ProductType type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.type = type;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Optional<String> getDescription() {
        if (description == null) {
            return Optional.empty();
        }
        return Optional.of(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
