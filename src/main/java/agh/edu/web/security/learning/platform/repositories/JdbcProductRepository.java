package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.mappers.ProductMapper;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class JdbcProductRepository implements ProductRepository {

    private static final Logger log = Logger.getLogger(JdbcProductRepository.class.getName());

    @Autowired
    private JdbcTemplate jdbc;

    public JdbcProductRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public List<Product> getAllUserProducts(String id) {
        var unsafeQuery = "SELECT p.* FROM products p join product_transaction pt on(p.id = pt.product_id) join users u on (pt.user_id = u.id) WHERE u.id =  " + id;
        log.info("Performed query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductMapper());
    }

    @Override
    public List<Product> getAllUserProductsWithoutUnion(String id) {
        var unsafeQuery = "SELECT p.* FROM products p join product_transaction pt on(p.id = pt.product_id) join users u on (pt.user_id = u.id) WHERE u.id =  " + id;
        if (unsafeQuery.toLowerCase().contains("union")) {
            return new LinkedList();
        }
        log.info("Performed query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductMapper());
    }

    @Override
    public Optional<Product> getProductById(String id) {
        var unsafeQuery = "SELECT * FROM products p WHERE p.id = " + id;
        Product product = null;
        try {
            log.log(Level.INFO,"Performed query: \n" + unsafeQuery);
            product = jdbc.queryForObject(unsafeQuery, new ProductMapper());
        } catch (Exception e) {
            log.log(Level.INFO, "Resource does not exist");
            log.log(Level.ALL, e.getMessage());
            return Optional.empty();
        }
        return Optional.of(product);
    }

    @Override
    public List<Product> getAllProductByNameForUser(String userId, String productName) {
        var unsafeQuery = "SELECT p.* FROM products p join product_transaction pt on(p.id = pt.product_id) join users u on (pt.user_id = u.id) WHERE p.name LIKE '%" + productName + "%' and u.id=" + userId ;
        log.info("Performed query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductMapper());
    }

    @Override
    public List<Product> getAllProductByNameForUserWithFilter(String userId, String productName) {
        if (productName.contains(" ")) {
            return new LinkedList<>();
        }
        return getAllProductByNameForUser(userId, productName);
    }

    @Override
    public List<Product> getAllUserProductByType(String userId, String type) {
        var unsafeQuery = "SELECT p.* FROM products p join product_transaction pt on(p.id = pt.product_id) join users u on (pt.user_id = u.id) WHERE u.id=" + userId  + " and p.type='" + type + "'";
        log.info("Performed query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductMapper());
    }

    @Override
    public List<Product> getAllUserProductSortedByColumn(String userId, String column) {
        var unsafeQuery = "SELECT p.* FROM products p join product_transaction pt on(p.id = pt.product_id) join users u on (pt.user_id = u.id) WHERE u.id=" + userId + " order by " + column;
        log.info("Performed query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductMapper());
    }

    @Override
    public List<Product> getAllProductOffersWithMappedParameters(HttpServletRequest request) {
        var unsafeQuery = new StringBuilder("SELECT p.* FROM products p join product_transaction pt on(p.id = pt.product_id) join users u on (pt.user_id = u.id) WHERE u.id=" + 1);

        var parameters = request.getParameterNames().asIterator();
        var list = new ArrayList<String>();
        parameters.forEachRemaining(list::add);

        for (var param: list) {
            String value = request.getParameter(param);
            if (value.isEmpty()) continue;
            String table = param.substring(param.indexOf('['));
            String column = param.substring(param.indexOf('[')+1, param.indexOf(']'));
            unsafeQuery.append(" AND p.").append(column).append("='").append(value).append("'");
        }
        log.info("Performed query: \n" + unsafeQuery.toString());
        return jdbc.query(unsafeQuery.toString(), new ProductMapper());
    }

    @Override
    public int saveProduct(Product product) {
        if (product.getDescription().isEmpty())
            product.setDescription("none");
        try {
            return jdbc.update("INSERT INTO products(name, price, description, type) VALUES(?, ?, ?, ?)", product.getName(), product.getPrice(), product.getDescription().get(), product.getType().toString());
        } catch (Exception e) {
            return -1;
        }
    }
}
