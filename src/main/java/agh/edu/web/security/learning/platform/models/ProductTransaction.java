package agh.edu.web.security.learning.platform.models;

import javax.persistence.*;

@Entity
@Table(name = "product_transaction")
public class ProductTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false, name = "product_id")
    private Long productId;
    @Column(nullable = false, name = "user_id")
    private Long userId;

    public ProductTransaction() {
    }

    public ProductTransaction(Long id, Long productId, Long userId) {
        this.id = id;
        this.productId = productId;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
