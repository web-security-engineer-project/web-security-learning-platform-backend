package agh.edu.web.security.learning.platform.utils;

import org.springframework.util.SystemPropertyUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

public class ExecuteOSCommandHelper {

    private static Logger log = Logger.getLogger(ExecuteOSCommandHelper.class.getName());

    public static String executeCommand(String command, String value) {
        var os = System.getProperty("os.name");
        log.info("OS NAME: " + os);
        if (os.contains("Windows")) {
            return executeCmdCommand(command, value);
        } else {
            return executeBashCommand(command, value);
        }
    }

    private static String executeCmdCommand(String command, String value) {
        try {
         //   ProcessBuilder processBuilder = new ProcessBuilder("cmd", "/c", command, value);
            Process process = null;
            if (command.contains("ping")) {
                 process = Runtime.getRuntime().exec("cmd /c ping -n 1 " +  value);
            } else {
                 process = Runtime.getRuntime().exec("cmd /c " + command + " " +  value);
            }
            return readCommandOutput(process);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private static String executeBashCommand(String command, String value) {
        try {
            var processBuilder = new ProcessBuilder();
            if (command.contains("ping")) {
                processBuilder.command("/bin/sh", "-c", "ping -c 1 " + value);
            } else {
                processBuilder.command("/bin/sh", "-c", command + " " + value);
            }
            var process = processBuilder.start();
            return readCommandOutput(process);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public static String readCommandOutput(Process process) throws InterruptedException, IOException {
        var inputStream = process.getInputStream();
        var bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        var buffer = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            buffer.append(line).append('\n');
        }
        var output = buffer.toString();
        process.waitFor();
        return output;
    }
}
