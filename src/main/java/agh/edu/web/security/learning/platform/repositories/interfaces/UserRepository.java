package agh.edu.web.security.learning.platform.repositories.interfaces;

import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.models.UserAgent;

import java.util.Optional;

public interface UserRepository {
    boolean userAuthorization(User user);
    Optional<User> findUserById(String id);
    void updateUserInfo(User user);
}
