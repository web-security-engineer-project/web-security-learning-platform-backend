package agh.edu.web.security.learning.platform.repositories.interfaces;


import agh.edu.web.security.learning.platform.models.Product;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    List<Product> getAllUserProducts(String id);
    List<Product> getAllUserProductsWithoutUnion(String id);
    Optional<Product> getProductById(String id);
    List<Product> getAllProductByNameForUser(String userId, String productName);
    List<Product> getAllProductByNameForUserWithFilter(String userId, String productName);
    List<Product> getAllUserProductByType(String userId, String type);
    List<Product> getAllUserProductSortedByColumn(String userId, String column);
    List<Product> getAllProductOffersWithMappedParameters(HttpServletRequest request);
    int saveProduct(Product product);
}
