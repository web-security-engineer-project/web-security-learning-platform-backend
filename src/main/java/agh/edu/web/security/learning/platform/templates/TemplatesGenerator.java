package agh.edu.web.security.learning.platform.templates;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import java.io.StringWriter;

public class TemplatesGenerator {

    public static String generateThymeleafTemplateExercise1(String color, String fontSize, String fontFamily) {
        var template = "<!DOCTYPE html>\n" +
                "<html xmlns:th=\"https://www.thymeleaf.org\" lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Web Security Training Platform</title>\n" +
                "    <!-- CSS only -->\n" +
                "    <link rel=\"stylesheet\" href=\"../css/boostrap.min.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"../css/comments.css\" />\n" +
                "    <!-- JS, Popper.js, and jQuery -->\n" +
                "    <script src=\"../javascript/jquery.slim.min.js\"></script>\n" +
                "    <script src=\"../javascript/popper.min.js\"></script>\n" +
                "    <script src=\"../javascript/boostrap.min.js\"></script>\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/styles.css\" />\n" +
                "    <script src=\"../../javascript/script.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div id=\"wrapper\" class=\"toggled\">\n" +
                "    <nav class=\"navbar navbar-inverse fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">\n" +
                "        <ul class=\"nav sidebar-nav\">\n" +
                "            <div class=\"sidebar-header\">\n" +
                "                <div class=\"sidebar-brand\">\n" +
                "                    <a href=\"/index\">Home</a></div>\n" +
                "            </div>\n" +
                "            <li><a href=\"/introduction\">Introduction</a></li>"+
                "            <li><a href=\"/injection\">Injection</a></li>\n" +
                "            <li><a href=\"/auth\">Broken Authorization</a></li>\n" +
                "            <li><a href=\"/xss\">Cross-site Scripting (XSS)</a></li>\n" +
                "            <li><a href=\"/xxe\">XML External Entity (XML)</a></li>\n" +
                "            <li><a href=\"/deserialization\">Deserialization</a></li>\n" +
                "            <li><a href=\"/path\">Path Traversal</a></li>\n" +
                "        </ul>\n" +
                "    </nav>\n" +
                "    <div id=\"page-content-wrapper\">\n" +
                "        <div class=\"container\">\n" +
                "            <div class=\"row\">\n" +
                "                <div class=\"col-lg-8 col-lg-offset-2\">\n" +
                "                    <h2>Display settings</h2>\n" +
                "                    <hr />\n" +
                "                        <p class=\"alert alert-info\" style=\"display: none\" id=\"hint\">Javascript code can be injected in HTML attributes. Look at GET request when you apply changes.</p>\n" +
                "                    <form method=\"get\" class=\"mb-2\" th:action=\"@{/xss/zad2}\">\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <label>Theme</label>\n" +
                "                            <select class=\"form-control\" name=\"color\">\n" +
                "                                <option>dark</option>\n" +
                "                                <option>white</option>\n" +
                "                            </select>\n" +
                "                        </div>\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <label>Size font</label>\n" +
                "                            <select class=\"form-control\" name=\"font-size\">\n" +
                "                                <option>small</option>\n" +
                "                                <option>medium</option>\n" +
                "                                <option>large</option>\n" +
                "                            </select>\n" +
                "                        </div>\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <label>Size font</label>\n" +
                "                            <select class=\"form-control\" name=\"font-family\">\n" +
                "                                <option>times</option>\n" +
                "                                <option>arial</option>\n" +
                "                                <option>courier</option>\n" +
                "                            </select>\n" +
                "                        </div>\n" +
                "                        <button type=\"submit\" class=\"btn btn-primary\">Change your page theme</button>\n" +
                "                    </form>\n" +
                "                    <p class=\"display-4\">" +
                "                    <div class=\"" + color + " border-dark mb-2\">\n" +
                "                        <div class=\"" + fontFamily + "\">\n" +
                "                            <div class=\"" + fontSize + "\">\n" +
                "                                example display\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                    </div></p>\n" +
                "                    <div class=\"row justify-content-between\">\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <nav aria-label=\"Page navigation example\">\n" +
                "                                <ul class=\"pagination\">\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/xss/zad1\" aria-label=\"Previous\">\n" +
                "                                            <span aria-hidden=\"true\">&laquo;</span>\n" +
                "                                            <span class=\"sr-only\">Previous</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad1\">1</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad2\">2</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad3\">3</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad4\">4</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad5\">5</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad6\">6</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad7\">7</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad10\">8</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad8\">9</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad9\">10</a></li>\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/xss/zad3\" aria-label=\"Next\">\n" +
                "                                            <span aria-hidden=\"true\">&raquo;</span>\n" +
                "                                            <span class=\"sr-only\">Next</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                </ul>\n" +
                "                            </nav>\n" +
                "                        </div>\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <button type=\"button\" onclick=\"showHint()\" class=\"btn btn-primary\">Show Hint</button>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        return template;
    }

    public static String generateThymeleafTemplateForExercise2(String src, String exercise) {
        String template = "<!DOCTYPE html>\n" +
                "<html xmlns:th=\"https://www.thymeleaf.org\" lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Web Security Training Platform</title>\n" +
                "    <!-- CSS only -->\n" +
                "    <link rel=\"stylesheet\" href=\"../css/boostrap.min.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"../css/comments.css\"/>\n" +
                "    <!-- JS, Popper.js, and jQuery -->\n" +
                "    <script src=\"../javascript/jquery.slim.min.js\"></script>\n" +
                "    <script src=\"../javascript/popper.min.js\"></script>\n" +
                "    <script src=\"../javascript/boostrap.min.js\"></script>\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/styles.css\"/>\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/iframe.css\"/>\n" +
                "    <script src=\"../../javascript/script.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div id=\"wrapper\" class=\"toggled\">\n" +
                "    <nav class=\"navbar navbar-inverse fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">\n" +
                "        <ul class=\"nav sidebar-nav\">\n" +
                "            <div class=\"sidebar-header\">\n" +
                "                <div class=\"sidebar-brand\">\n" +
                "                    <a href=\"/index\">Home</a></div>\n" +
                "            </div>\n" +
                "            <li><a href=\"/introduction\">Introduction</a></li>"+
                "            <li><a href=\"/injection\">Injection</a></li>\n" +
                "            <li><a href=\"/auth\">Broken Authorization</a></li>\n" +
                "            <li><a href=\"/xss\">Cross-site Scripting (XSS)</a></li>\n" +
                "            <li><a href=\"/xxe\">XML External Entity (XML)</a></li>\n" +
                "            <li><a href=\"/deserialization\">Deserialization</a></li>\n" +
                "            <li><a href=\"/path\">Path Traversal</a></li>\n" +
                "        </ul>\n" +
                "    </nav>\n" +
                "    <div id=\"page-content-wrapper\">\n" +
                "        <div class=\"container\">\n" +
                "            <div class=\"row\">\n" +
                "                <div class=\"col-lg-8 col-lg-offset-2\">\n" +
                "                    <h2>Kittens Gallery</h2>\n" +
                "                    <hr/>\n" +
                "                    <p class=\"alert alert-info\" style=\"display: none\" id=\"hint\">Check out url when you are requesting kitten image.</p>\n" +
                "                    <div class=\"container\">\n" +
                "                        <div class=\"row justify-content-between\">\n" +
                "                            <div class=\"col-auto align-items-center\">\n" +
                "                                <form method=\"get\" class=\"mb-3\" action=\"" + exercise + "\">\n" +
                "                                    <input type=\"hidden\" name=\"cat\" value=\"../../cat1.webp\">\n" +
                "                                    <button style=\"width: 200px;\" type=\"submit\" class=\"btn btn-primary\">Show cute cat</button>\n" +
                "                                </form>\n" +
                "                                <form method=\"get\" class=\"mb-3\" action=\"" + exercise + "\">\n" +
                "                                    <input type=\"hidden\" name=\"cat\" value=\"../../cat2.webp\">\n" +
                "                                    <button style=\"width: 200px;\"  type=\"submit\" class=\"btn btn-primary\">Show other cute cat</button>\n" +
                "                                </form>\n" +
                "                                <form method=\"get\" class=\"mb-2\" action=\"" + exercise + "\">\n" +
                "                                    <input type=\"hidden\" name=\"cat\" value=\"../../cat3.webp\">\n" +
                "                                    <button style=\"width: 200px;\"  type=\"submit\" class=\"btn btn-primary\">Show pawsome cat</button>\n" +
                "                                </form>\n" +
                "                            </div>\n" +
                "                            <div class=\"col-auto\">\n" +
                "                                <div class=\"wrap\">\n" +
                "                                <iframe class=\"frame\" scrolling=\"no\" src=\"" + src +"\" height=\"200\" width=\"200\" alt=\"Error cat didn't fetch\"></iframe>\n" +
                "                                </div>\n" +
                "                                </div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <div class=\"row justify-content-between\" style=\"margin-top: -150px\">\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <nav aria-label=\"Page navigation example\">\n" +
                "                                <ul class=\"pagination\">\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/path/zad1\" aria-label=\"Previous\">\n" +
                "                                            <span aria-hidden=\"true\">&laquo;</span>\n" +
                "                                            <span class=\"sr-only\">Previous</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/path/zad1\">1</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/path/zad2\">2</a></li>\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/path/zad2\" aria-label=\"Next\">\n" +
                "                                            <span aria-hidden=\"true\">&raquo;</span>\n" +
                "                                            <span class=\"sr-only\">Next</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                </ul>\n" +
                "                            </nav>\n" +
                "                        </div>\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <button type=\"button\" onclick=\"showHint()\" class=\"btn btn-primary\">Show Hint</button>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        return template;
    }

    public static String generateTemplateForXSSExcercsie(String src) {
        String template = "<!DOCTYPE html>\n" +
                "<html xmlns:th=\"https://www.thymeleaf.org\" lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Web Security Training Platform</title>\n" +
                "    <!-- CSS only -->\n" +
                "    <link rel=\"stylesheet\" href=\"../css/boostrap.min.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"../css/comments.css\"/>\n" +
                "    <!-- JS, Popper.js, and jQuery -->\n" +
                "    <script src=\"../javascript/jquery.slim.min.js\"></script>\n" +
                "    <script src=\"../javascript/popper.min.js\"></script>\n" +
                "    <script src=\"../javascript/boostrap.min.js\"></script>\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/styles.css\"/>\n" +
                "    <script src=\"../../javascript/script.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div id=\"wrapper\" class=\"toggled\">\n" +
                "    <nav class=\"navbar navbar-inverse fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">\n" +
                "        <ul class=\"nav sidebar-nav\">\n" +
                "            <div class=\"sidebar-header\">\n" +
                "                <div class=\"sidebar-brand\">\n" +
                "                    <a href=\"/index\">Home</a></div>\n" +
                "            </div>\n" +
                "            <li><a href=\"/introduction\">Introduction</a></li>"+
                "            <li><a href=\"/injection\">Injection</a></li>\n" +
                "            <li><a href=\"/auth\">Broken Authorization</a></li>\n" +
                "            <li><a href=\"/xss\">Cross-site Scripting (XSS)</a></li>\n" +
                "            <li><a href=\"/xxe\">XML External Entity (XML)</a></li>\n" +
                "            <li><a href=\"/deserialization\">Deserialization</a></li>\n" +
                "            <li><a href=\"/path\">Path Traversal</a></li>\n" +
                "        </ul>\n" +
                "    </nav>\n" +
                "    <div id=\"page-content-wrapper\">\n" +
                "        <div class=\"container\">\n" +
                "            <div class=\"row\">\n" +
                "                <div class=\"col-lg-8 col-lg-offset-2\">\n" +
                "                    <h2>Kittens Gallery</h2>\n" +
                "                    <hr/>\n" +
                "                    <p class=\"alert alert-info\" style=\"display: none\" id=\"hint\">Try to modify url, in order to\n" +
                "                        trigger XSS</p>\n" +
                "                    <div class=\"container\">\n" +
                "                        <div class=\"row justify-content-between align-items-center\">\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <form method=\"get\" class=\"mb-3\" action=\"/xss/zad3\">\n" +
                "                                <input type=\"hidden\" name=\"cat\" value=\"../img/cat1.webp\">\n" +
                "                                <button style=\"width: 200px;\" type=\"submit\" class=\"btn btn-primary\">Show cute cat</button>\n" +
                "                            </form>\n" +
                "                            <form method=\"get\" class=\"mb-3\" action=\"/xss/zad3\">\n" +
                "                                <input type=\"hidden\" name=\"cat\" value=\"../img/cat2.webp\">\n" +
                "                                <button style=\"width: 200px;\"  type=\"submit\" class=\"btn btn-primary\">Show other cute cat</button>\n" +
                "                            </form>\n" +
                "                            <form method=\"get\" class=\"mb-2\" action=\"/xss/zad3\">\n" +
                "                                <input type=\"hidden\" name=\"cat\" value=\"../img/cat3.webp\">\n" +
                "                                <button style=\"width: 200px;\"  type=\"submit\" class=\"btn btn-primary\">Show pawsome cat</button>\n" +
                "                            </form>\n" +
                "                        </div>\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <img src=\"" + src + "\" height=\"200\" alt=\"Error cat didn't fetch\"/>\n" +
                "                        </div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <div class=\"row justify-content-between mt-2\">\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <nav aria-label=\"Page navigation example\">\n" +
                "                                <ul class=\"pagination\">\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/xss/zad2\" aria-label=\"Previous\">\n" +
                "                                            <span aria-hidden=\"true\">&laquo;</span>\n" +
                "                                            <span class=\"sr-only\">Previous</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad1\">1</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad2\">2</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad3\">3</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad4\">4</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad5\">5</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad6\">6</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad7\">7</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad10\">8</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad8\">9</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/xss/zad9\">10</a></li>\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/xss/zad4\" aria-label=\"Next\">\n" +
                "                                            <span aria-hidden=\"true\">&raquo;</span>\n" +
                "                                            <span class=\"sr-only\">Next</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                </ul>\n" +
                "                            </nav>\n" +
                "                        </div>\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <button type=\"button\" onclick=\"showHint()\" class=\"btn btn-primary\">Show Hint</button>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        return template;
    }
}
