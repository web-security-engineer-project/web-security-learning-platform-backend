package agh.edu.web.security.learning.platform.models;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

public class VulnerableEntity {

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
    public Object vulnerableField;

    public String value;
}
