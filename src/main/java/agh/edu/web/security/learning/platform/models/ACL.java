package agh.edu.web.security.learning.platform.models;

public enum ACL {
   ADMIN, USER;
}
