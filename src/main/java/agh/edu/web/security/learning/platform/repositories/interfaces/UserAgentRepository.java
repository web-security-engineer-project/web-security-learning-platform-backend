package agh.edu.web.security.learning.platform.repositories.interfaces;

import agh.edu.web.security.learning.platform.models.UserAgent;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface UserAgentRepository {
    Optional<UserAgent> findUserUserAgent(String id);
    void saveUserAgent(String id, String name);
}
