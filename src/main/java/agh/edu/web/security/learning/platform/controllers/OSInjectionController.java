package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.utils.ExecuteOSCommandHelper;
import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import org.hibernate.annotations.GeneratorType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

@Controller
public class OSInjectionController {

    private static final Logger log = Logger.getLogger(OSInjectionController.class.getName());

    @GetMapping({"/injection/os/simple"})
    public String getOSInjectionSimple(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/injection/os/simple";
    }

    @PostMapping({"/injection/os/simple"})
    public ModelAndView processOSInjectionSimple(@RequestParam(value = "ip") String ip, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/os/simple");
        var output = ExecuteOSCommandHelper.executeCommand("nslookup", ip);
        log.info("OS Command output: " + output);
        modelAndView.addObject("output", output);
        return modelAndView;
    }

    @GetMapping({"/injection/os/blind"})
    public String getOSInjectionBlind(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/injection/os/blind";
    }

    @PostMapping({"/injection/os/blind"})
    public ModelAndView postOSInjectionBlind(@RequestParam(value = "ip") String ip, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/injection/os/blind");
        var output = ExecuteOSCommandHelper.executeCommand("ping -n 1", ip);
        log.info("OS Command output: " + output);
        if (output.contains("Received = 1") || output.contains("1 received")){
            output = "message sent";
        } else {
            output = "problem with message delivery";
        }
        modelAndView.addObject("output", output);
        return modelAndView;
    }
}
