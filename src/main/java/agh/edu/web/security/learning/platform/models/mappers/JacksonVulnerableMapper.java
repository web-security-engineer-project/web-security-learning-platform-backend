package agh.edu.web.security.learning.platform.models.mappers;

import agh.edu.web.security.learning.platform.models.VulnerableEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JacksonVulnerableMapper {

    private static ObjectMapper mapper = new ObjectMapper();

    public static VulnerableEntity parseJson(String json) throws JsonProcessingException, IOException {
        mapper.enableDefaultTyping();
        return mapper.readValue(json, VulnerableEntity.class);
    }
}
