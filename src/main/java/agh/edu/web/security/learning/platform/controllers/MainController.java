package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
public class MainController {

    @GetMapping({"/", "/index", "/index.html"})
    public String index(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "index";
    }
}
