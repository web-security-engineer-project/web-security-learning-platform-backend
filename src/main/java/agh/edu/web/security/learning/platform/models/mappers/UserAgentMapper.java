package agh.edu.web.security.learning.platform.models.mappers;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.UserAgent;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserAgentMapper implements RowMapper<UserAgent> {
    @Override
    public UserAgent mapRow(ResultSet rs, int rowNum) throws SQLException {
        var userAgent = new UserAgent();
        userAgent.setUserAgentName(rs.getString("user_agent"));
        userAgent.setUserId(rs.getInt("user_id"));
        return userAgent;
    }
}
