package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.models.Comment;
import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductRepository;
import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import agh.edu.web.security.learning.platform.utils.XMLParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class XXEController {

    private static final Logger log = Logger.getLogger(XXEController.class.getName());

    private XMLParser parser = new XMLParser();
    private List<Comment> comments = new ArrayList<>();

    @Autowired
    ProductRepository productRepository;

    public XXEController(ProductRepository productRepository) {
        this.productRepository = productRepository;
        comments.add(new Comment("What a great blog", "Maria"));
        comments.add(new Comment("How we should interpret that?", "Zoe"));
    }

    public XXEController() {
        comments.add(new Comment("What a great blog", "Maria"));
        comments.add(new Comment("How we should interpret that?", "Zoe"));
    }

    @RequestMapping({"/xxe/success"})
    public String getSuccess(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/xxe/success";
    }

    @RequestMapping({"/xxe"})
    public String injection(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/xxe";
    }

    @GetMapping({"/xxe/zad1"})
    public String getExercise1(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/xxe/zad1";
    }

    @PostMapping(value = {"/xxe/addProduct"})
    @ResponseBody
    public int processXMLExercise1(@RequestBody String newProduct, @RequestHeader("Content-type") String contentType, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        int result = -1;
        log.log(Level.ALL,
                "IA M HERE");
        if (contentType.contains(MediaType.APPLICATION_XML_VALUE)) {
            try {
                var product = parser.parseProduct(newProduct);
                result = productRepository.saveProduct(product);
            } catch (JAXBException | XMLStreamException e) {
                e.printStackTrace();
                result = -2;
                log.log(Level.ALL, e.getMessage());
            }
        }
        response.addHeader("Access-Control-Allow-Origin", "*");
        return result;
    }

    @PostMapping(value = {"/xxe/addComment"})
    @ResponseBody
    public int processXMLExercise2(@RequestBody String newComment, @RequestHeader("Content-type") String contentType, HttpServletResponse response) {
        int result = -1;
        log.log(Level.ALL,
                "IA M HERE");
        if (contentType.contains(MediaType.APPLICATION_XML_VALUE)) {
            try {
                var comment = parser.parseXml(newComment);
                comment.setUser("Hacker");
                comments.add(comment);
            } catch (JAXBException | XMLStreamException e) {
                e.printStackTrace();
                result = -2;
                log.log(Level.ALL, e.getMessage());
            }
        }
        response.addHeader("Access-Control-Allow-Origin", "*");
        return result;
    }

    @RequestMapping({"/xxe/zad2"})
    public ModelAndView getExercise2(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView("src/xxe/zad2");
        modelAndView.addObject("comments", comments);
        return modelAndView;
    }
}
