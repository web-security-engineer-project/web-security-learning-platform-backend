package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.models.ACL;
import agh.edu.web.security.learning.platform.models.Comment;
import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import agh.edu.web.security.learning.platform.utils.JWTTokenBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.spec.SecretKeySpec;
import javax.print.DocFlavor;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping({"/jwt"})
public class JWTController {

    private List<Comment> comments = new ArrayList<>();
    private List<Comment> comments2 = new ArrayList<>();
    private JWTTokenBuilder tokenBuilder = new JWTTokenBuilder();
    private String token = tokenBuilder.generateTokenForBruteForce("guest", ACL.USER);

    public JWTController() {
        comments.add(new Comment("I don't like what you post.", "Maria"));
        comments.add(new Comment("Why do you we don't like it.", "Zoe"));
        comments.add(new Comment("Please do something with yourself.", "Zoe"));

        comments2.add(new Comment("Oh my god he is again talking about it.", "Maria"));
        comments2.add(new Comment("Please, stop posting on this blog.", "Zoe"));
        comments2.add(new Comment("Your posts make me sick.", "Zoe"));
    }

    @RequestMapping({"/zad1"})
    public ModelAndView getExercise1(HttpServletResponse response) {
        response.addHeader("Referrer-Policy", "no-referrer");
        response.addHeader("Access-Control-Allow-Origin", "*");
        var modelAndView = new ModelAndView("src/auth/jwt/zad1");
        response.addCookie(new Cookie("token", tokenBuilder.generateSecureToken("hacker", ACL.USER)));
        modelAndView.addObject("comments", comments);
        return modelAndView;
    }

    @DeleteMapping({"/zad2/deleteComment/{id}"})
    public ResponseEntity<?> deleteComment(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token, HttpServletResponse response) throws IOException {
        response.addHeader("Referrer-Policy", "no-referrer");
        response.addHeader("Access-Control-Allow-Origin", "*");
        if (token.contains(".")  && token.split("\\.").length == 3) {
            var jwt = token.split("\\.");
            var secret = jwt[2];
            var actualSecret = tokenBuilder.generateSecureTokenWithDifferentSecret("hacker", ACL.USER).split("\\.")[2];
            if (!secret.equals(actualSecret)) {
                return ResponseEntity.status(401).body("JWT tokens secrets are invalid: " + secret + " should be " + actualSecret);
            }
            var base64Body = jwt[1];
            var header = new String(Base64.getDecoder().decode(base64Body));
            var map = new ObjectMapper().readValue(header, HashMap.class);
            if (map.containsKey("ACL")) {
                var role = (String) map.get("ACL");
                if (ACL.valueOf(role) == ACL.ADMIN) {
                    if (comments.size() > id)
                    comments.remove(id -1);
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.status(401).body("Unauthorized");
                }
            } else {
                return ResponseEntity.status(401).body("Unauthorized");
            }

        } else {
            return ResponseEntity.badRequest().body("invalid jwt token");
        }
    }

    @RequestMapping({"/zad2"})
    public ModelAndView getExercise2(HttpServletResponse response) {
        response.addHeader("Referrer-Policy", "no-referrer");
        response.addHeader("Access-Control-Allow-Origin", "*");
        var modelAndView = new ModelAndView("src/auth/jwt/zad2");
        response.addCookie(new Cookie("token", tokenBuilder.generateSecureToken("hacker", ACL.USER)));
        modelAndView.addObject("comments", comments);
        return modelAndView;
    }

    @DeleteMapping({"/zad1/deleteComment/{id}"})
    public ResponseEntity<?> deleteComment2(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token, HttpServletResponse response) throws IOException {
        response.addHeader("Referrer-Policy", "no-referrer");
        response.addHeader("Access-Control-Allow-Origin", "*");
        if (token.contains(".") && token.split("\\.").length == 3) {
            var jwt = token.split("\\.");

            var base64Body = jwt[1];
            var header = new String(Base64.getDecoder().decode(base64Body));
            var map = new ObjectMapper().readValue(header, HashMap.class);
            if (map.containsKey("ACL")) {
                var role = (String) map.get("ACL");
                if (ACL.valueOf(role) == ACL.ADMIN) {
                    if (comments.size() > id)
                        comments.remove(id -1);
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.status(401).body("Unauthorized");
                }
            } else {
                return ResponseEntity.status(401).body("Unauthorized");
            }

        } else {
            return ResponseEntity.badRequest().body("invalid jwt token");
        }
    }

    @GetMapping({"/zad3"})
    public String getExercise3(Model model) {
        model.addAttribute("token", token);
        return "src/auth/jwt/zad3";
    }

    @PostMapping({"/zad3/crack"})
    public ResponseEntity crackPassword(@RequestBody String token) {
        if (token.contains(".") && token.split("\\.").length == 3) {
            var jwt = token.split("\\.");
            var base64Body = jwt[1];
            var header = new String(Base64.getDecoder().decode(base64Body));
            HashMap map = null;
            try {
                map = new ObjectMapper().readValue(header, HashMap.class);
            } catch (JsonProcessingException e) {
              //
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (map != null && map.containsKey("ACL")) {
                var user = (String) map.get("user");
                var role = (String) map.get("ACL");
                if (ACL.valueOf(role) == ACL.ADMIN && user.equals("hacker")) {
                    Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(JWTTokenBuilder.JWT_SECRET_HARD),
                            SignatureAlgorithm.HS256.getJcaName());
                    try {
                        Jws<Claims> claims = Jwts.parser()
                                .setSigningKey(hmacKey)
                                .parseClaimsJws(token);
                    } catch (SignatureException e) {
                        return ResponseEntity.status(401).body("Unauthorized");
                    }
                    return ResponseEntity.status(200).body("Success!");
                } else {
                    return ResponseEntity.status(401).body("Unauthorized");
                }
            } else {
                return ResponseEntity.status(401).body("Unauthorized");
            }

        } else {
            return ResponseEntity.badRequest().body("invalid jwt token");
        }
    }
}
