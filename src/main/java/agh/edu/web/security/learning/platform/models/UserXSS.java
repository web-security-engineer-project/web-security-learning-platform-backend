package agh.edu.web.security.learning.platform.models;

public class UserXSS {

    private String accountName;
    private String email;
    private String specialities;
    private String linkedin;
    private String github;

    public UserXSS(String accountName, String email, String specialities, String linkedin, String github) {
        this.accountName = accountName;
        this.email = email;
        this.specialities = specialities;
        this.linkedin = linkedin;
        this.github = github;
    }

    public void updateUser(UserXSS userXSS) {
        if (userXSS.accountName != null && !userXSS.accountName.isEmpty()) {
            this.accountName = userXSS.accountName;
        }
        if (userXSS.email != null && !userXSS.email.isEmpty()) {
            this.email = userXSS.email;
        }
        if (userXSS.specialities != null && !userXSS.specialities.isEmpty()) {
            this.specialities = userXSS.specialities;
        }
        if (userXSS.linkedin != null && !userXSS.linkedin.isEmpty()) {
            this.linkedin = userXSS.linkedin;
        }
        if (userXSS.github != null && !userXSS.github.isEmpty()) {
            this.github = userXSS.github;
        }
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getGithub() {
        return github;
    }

    public void setGithub(String github) {
        this.github = github;
    }
}
