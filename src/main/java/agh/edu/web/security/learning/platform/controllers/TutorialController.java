package agh.edu.web.security.learning.platform.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/introduction")
public class TutorialController {

    @GetMapping("")
    public String getIntroductionPage() {
        return "src/introduction/index";
    }

    @GetMapping("/tutorial/broken-auth")
    public String getBrokenAuthPage() {
        return "src/introduction/tutorial/broken-auth";
    }

    @GetMapping("/tutorial/deserialization")
    public String getDeserializationPage() {
        return "src/introduction/tutorial/deserialization";
    }

    @GetMapping("/tutorial/idor")
    public String getIDORPage() {
        return "src/introduction/tutorial/idor";
    }

    @GetMapping("/tutorial/jwt")
    public String getJWTPage() {
        return "src/introduction/tutorial/jwt";
    }

    @GetMapping("/tutorial/os")
    public String getCommandInjectionPage() {
        return "src/introduction/tutorial/os";
    }

    @GetMapping("/tutorial/sql")
    public String getSQLInjectionPage() {
        return "src/introduction/tutorial/sql";
    }

    @GetMapping("/tutorial/traversal")
    public String getPathTraversalPage() {
        return "src/introduction/tutorial/traversal";
    }

    @GetMapping("/tutorial/xss")
    public String getXSSPage() {
        return "src/introduction/tutorial/xss";
    }

    @GetMapping("/tutorial/xxe")
    public String getXXEPage() {
        return "src/introduction/tutorial/xxe";
    }

    @GetMapping("/tutorial/ssti")
    public String getServerSideTemplateInjectionPage() {
        return "src/introduction/tutorial/ssti";
    }
}
