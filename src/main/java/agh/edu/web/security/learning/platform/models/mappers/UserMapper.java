package agh.edu.web.security.learning.platform.models.mappers;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        var user = new User();
        user.setId(rs.getLong("id"));
        user.setName(rs.getString("name"));
        user.setPassword(rs.getString("password"));
        user.setEmail(rs.getString("email"));
        user.setPersonalInformation(rs.getString("personal_information"));
        return user;
    }
}
