package agh.edu.web.security.learning.platform.models.mappers;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements RowMapper<Product> {
    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        var product = new Product();
        product.setId(rs.getLong("id"));
        product.setName(rs.getString("name"));
        product.setPrice(rs.getString("price"));
        product.setDescription(rs.getString("description"));
        product.setType(ProductType.valueOf(rs.getString("type")));
        return product;
    }
}
