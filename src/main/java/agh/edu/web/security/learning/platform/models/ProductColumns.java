package agh.edu.web.security.learning.platform.models;

public enum ProductColumns {
    NAME("Book", "2"), PRICE("Price", "3"), DESCRIPTION("Description", "4"), TYPE("Type", "5");

    private final String display;
    private final String number;

    ProductColumns(String display, String number) {
        this.display = display;
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public String getDisplay() {
        return display;
    }
}
