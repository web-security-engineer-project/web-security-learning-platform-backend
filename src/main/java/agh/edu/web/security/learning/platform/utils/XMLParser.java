package agh.edu.web.security.learning.platform.utils;

import agh.edu.web.security.learning.platform.models.Comment;
import agh.edu.web.security.learning.platform.models.Product;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;


public class XMLParser {

    public Comment parseXml(String xml) throws JAXBException, XMLStreamException {
        var jaxbContext = JAXBContext.newInstance(Comment.class);
        var xmlInputFactory = XMLInputFactory.newInstance();
        var xsr = xmlInputFactory.createXMLStreamReader(new StringReader(xml));
        var unmarshaller = jaxbContext.createUnmarshaller();
        return (Comment) unmarshaller.unmarshal(xsr);
    }

    public Product parseProduct(String xml) throws JAXBException, XMLStreamException {
        var jc = JAXBContext.newInstance(Product.class);
        var xif = XMLInputFactory.newInstance();
        var xsr = xif.createXMLStreamReader(new StringReader(xml));
        var unmarshaller = jc.createUnmarshaller();
        return (Product) unmarshaller.unmarshal(xsr);
    }
}
