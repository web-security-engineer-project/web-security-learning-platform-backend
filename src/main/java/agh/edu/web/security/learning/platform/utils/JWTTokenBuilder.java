package agh.edu.web.security.learning.platform.utils;

import agh.edu.web.security.learning.platform.models.ACL;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

import java.util.Date;
import java.util.Random;

public class JWTTokenBuilder {

    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    SignatureAlgorithm weakAlgorithm = SignatureAlgorithm.NONE;
    public static final String[] SECRETS = {"hey", "you", "random", "secret", "key"};
    public static final String[] SECRETS2 = {"advise", "cry", "cup", "news", "offense"};
    public static final String JWT_SECRET = TextCodec.BASE64.encode(SECRETS[new Random().nextInt(SECRETS.length)]);
    public static final String JWT_SECRET_HARD = TextCodec.BASE64.encode(SECRETS2[new Random().nextInt(SECRETS.length)]);

    public String generateSecureToken(String user, ACL acl) {
        return Jwts.builder()
                .claim("user", user)
                .claim("ACL", acl.toString())
                .signWith(signatureAlgorithm, JWT_SECRET)
                .compact();
    }

    public String generateSecureTokenWithDifferentSecret(String user, ACL acl) {
        return Jwts.builder()
                .claim("user", user)
                .claim("ACL", acl.toString())
                .signWith(signatureAlgorithm, JWT_SECRET_HARD)
                .compact();
    }

    public String generateTokenForBruteForce(String user, ACL acl) {
        return Jwts.builder()
                .setExpiration(new Date(System.currentTimeMillis() + 600_000))
                .claim("user", user)
                .claim("ACL", acl.toString())
                .signWith(signatureAlgorithm, JWT_SECRET_HARD)
                .compact();
    }
}
