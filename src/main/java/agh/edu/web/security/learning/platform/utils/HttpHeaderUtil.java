package agh.edu.web.security.learning.platform.utils;

import javax.servlet.http.HttpServletResponse;

public class HttpHeaderUtil {
    public static void addHeaders(HttpServletResponse httpServletResponse) {
        httpServletResponse.addHeader("X-XSS-Protection","1; mode=block 1");
        httpServletResponse.addHeader("X-Frame-Options","SAMEORIGIN 1");
        httpServletResponse.addHeader("X-Content-Type-Options","nosniff");
       // httpServletResponse.addHeader("Content-Security-Policy","default-src 'self';script-src 'self' *.jquery.com *.jsdelivr.net stackpath.bootstrapcdn.com localhost data:;style-src 'self' stackpath.bootstrapcdn.com  data:; image-src 'self' stackpath.bootstrapcdn.com data:;");
        httpServletResponse.addHeader("Referrer-Policy","strict-origin-when-cross-origin");
    }
}
