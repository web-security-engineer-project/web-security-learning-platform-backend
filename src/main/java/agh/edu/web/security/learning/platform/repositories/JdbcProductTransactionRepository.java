package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductTransaction;
import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.models.mappers.ProductMapper;
import agh.edu.web.security.learning.platform.models.mappers.ProductTransactionMapper;
import agh.edu.web.security.learning.platform.models.mappers.UserMapper;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class JdbcProductTransactionRepository implements ProductTransactionRepository {

    private static final Logger log = Logger.getLogger(JdbcProductTransactionRepository.class.getName());

    @Autowired
    private JdbcTemplate jdbc;

    public JdbcProductTransactionRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public List<User> getProductBuyers(String id) {
        var unsafeQuery = "SELECT u.* FROM users u join product_transaction pt on (u.id = pt.user_id) join products p on (p.id = pt.product_id) where p.id = " + id;
        log.log(Level.INFO, "Prepared Query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new UserMapper());
    }

    @Override
    public List<Product> getUserProducts(String id) {
        var unsafeQuery = "SELECT p.* FROM products p join product_transaction pt on (p.id = pt.product_id) join users u on (u.id = pt.user_id) where u.id = " + id;
        log.log(Level.INFO, "Prepared Query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductMapper());
    }

    @Override
    public List<ProductTransaction> getTransactionByUserId(String id) {
        var unsafeQuery = "SELECT pt.* FROM product_transaction pt join users u on (pt.user_id = u.id) where u.id = " + id;
        log.log(Level.INFO, "Prepared Query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductTransactionMapper());
    }

    @Override
    public List<ProductTransaction> getTransactionByProductId(String id) {
        var unsafeQuery = "SELECT pt.* FROM product_transaction pt join products p on (pt.product_id = p.id) where p.id = " + id;
        log.log(Level.INFO, "Prepared Query: \n" + unsafeQuery);
        return jdbc.query(unsafeQuery, new ProductTransactionMapper());
    }
}
