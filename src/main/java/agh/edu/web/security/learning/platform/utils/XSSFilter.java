package agh.edu.web.security.learning.platform.utils;

public class XSSFilter {

    public static String filterXSS1(String xss) {
        if (xss.matches("^<script>.*</script>$")) {
            return "error";
        } else {
            return xss;
        }
    }

    public static String filterXSS2(String xss) {
        return xss.replace("(", "").replace(")", "");
    }
}
