package agh.edu.web.security.learning.platform.models;

public enum ProductType {
    BOOK("Book"), ELECTRONIC("Electronic"), FOOD("food");

    private final String display;

    ProductType(String display) {
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }
}
