package agh.edu.web.security.learning.platform.repositories;

import agh.edu.web.security.learning.platform.models.UserAgent;
import agh.edu.web.security.learning.platform.models.mappers.UserAgentMapper;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserAgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Logger;

@Repository
public class JdbcUserAgentRepository implements UserAgentRepository {

    private static final Logger log = Logger.getLogger(JdbcUserAgentRepository.class.getName());

    @Autowired
    private JdbcTemplate jdbc;

    public JdbcUserAgentRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Optional<UserAgent> findUserUserAgent(String id) {
        var unsafeQuery = "SELECT ua.* FROM users u join user_agent ua on (u.id=ua.user_id) WHERE u.id =" + id;
        try {
            var userAgent = jdbc.queryForObject(unsafeQuery, new UserAgentMapper());
            return Optional.of(userAgent);
        } catch (Exception e) {
            log.info("Performed query: \n" + unsafeQuery);
            log.info(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public void saveUserAgent(String id, String name) {
        var unsafeQuery = "UPDATE user_agent SET user_agent='" + name + "' WHERE user_id=" + id;
        try {
            log.info("Unsafe query \n" + unsafeQuery);
            jdbc.execute(unsafeQuery);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }
}
