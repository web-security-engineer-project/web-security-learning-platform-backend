package agh.edu.web.security.learning.platform.models.mappers;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.ProductTransaction;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductTransactionMapper implements RowMapper<ProductTransaction> {
    @Override
    public ProductTransaction mapRow(ResultSet rs, int rowNum) throws SQLException {
        var productTransaction = new ProductTransaction();
        productTransaction.setId(rs.getLong("id"));
        productTransaction.setUserId(rs.getLong("user_id"));
        productTransaction.setProductId(rs.getLong("product_id"));
        return productTransaction;
    }
}
