package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.models.Comment;
import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.models.UserXSS;
import agh.edu.web.security.learning.platform.templates.TemplatesGenerator;
import agh.edu.web.security.learning.platform.utils.GenerateFakeApiKey;
import agh.edu.web.security.learning.platform.utils.XSSFilter;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class XSSController {

    private static final Logger log = Logger.getLogger(XSSController.class.getName());
    private UserXSS userXSS = new UserXSS("John doe", "john@doe.mail", "Java, Spring, Cyber-Secrity", "https://www.linkedin.com", "https://github.com");
    private String profilePicture;
    private String extension;
    private List<Comment> comments = new ArrayList<>();

    @RequestMapping({"/test"})
    public String getDisplay() {
        return "src/xss/zad3";
    }

    public XSSController() {
        comments.add(new Comment("What a great blog", "Maria"));
        comments.add(new Comment("How we should interpret that?", "Zoe"));
    }

    @GetMapping({"/xss/profile-preview"})
    public String profilePreview(Model model) {
        displayPicture(model);
        model.addAttribute("user", this.userXSS);
        return "src/xss/profile-preview";
    }

    @GetMapping({"/xss"})
    public String mainXSSPage() {
        return "src/xss";
    }

    @GetMapping({"/xss/zad1"})
    public String getExercise1() {
        return "src/xss/zad1";
    }

    @GetMapping({"/xss/zad2"})
    @ResponseBody
    public String getExercise2(@RequestParam(value = "color", required = false) String color,
                               @RequestParam(value = "font-size", required = false) String fontSize,
                               @RequestParam(value = "font-family", required = false) String fontFamily,
                               Model model) {
        color =  (color != null && !color.isBlank()) ? color : "black";
        fontSize = (fontSize != null && !fontSize.isBlank()) ? fontSize : "medium";
        fontFamily =  (fontFamily != null && !fontFamily.isBlank()) ? fontFamily : "serif";
        return TemplatesGenerator.generateThymeleafTemplateExercise1(color, fontSize, fontFamily);
    }

    @PostMapping({"/xss/api-key"})
    public String generateApiKey(@RequestParam(value = "username") String username, Model model) {
        model.addAttribute("username", username);
        try {
            model.addAttribute("key", GenerateFakeApiKey.generate(128));
        } catch (Exception e) {
            log.log(Level.SEVERE, "Generating FakeApi key failed!!!");
            model.addAttribute("key", "Xx12321DASDG!~@DADA@WSDA");
        }
        return "src/xss/api";
    }

    @GetMapping({"/xss/zad3"})
    @ResponseBody
    public String getExercise3(@RequestParam(value = "cat", required = false)String src, HttpServletResponse response) {
        response.setHeader("X-Frame-Options", "DENY");
        src = (src ==null || src.isEmpty()) ? "/img/cat1.webp" : URLDecoder.decode(src);
        return TemplatesGenerator.generateTemplateForXSSExcercsie(src);
    }

    @GetMapping({"/xss/zad4"})
    public String getExercise4(HttpServletResponse response, Model model) {
        model.addAttribute("user", userXSS);
        return "src/xss/zad4";
    }

    @PostMapping({"/xss/updateProfile"})
    public String updateProfile(@ModelAttribute(value = "user") UserXSS userXSS,  HttpServletResponse response, Model model) {
        this.userXSS.updateUser(userXSS);
        model.addAttribute("user", this.userXSS);
        displayPicture(model);
        return "src/xss/profile-preview";
    }

    @GetMapping({"/xss/zad5"})
    public String getUpdateProfilePicturePage(Model model) {
        displayPicture(model);
        return "src/xss/zad5";
    }

    @PostMapping({"/xss/updateProfilePicture"})
    public String updateProfilePicture(@RequestParam(value = "picture")MultipartFile[] multipartFiles, HttpServletResponse response, Model model) {
        if (multipartFiles.length > 0) {
            try {
                profilePicture = Base64.encodeBase64String(multipartFiles[0].getBytes());
                if (multipartFiles[0].getOriginalFilename().contains(".")) {
                    extension = multipartFiles[0].getOriginalFilename().split("\\.")[1];
                    if (extension.equals("svg")) {
                        profilePicture = new String(multipartFiles[0].getBytes());
                    }
                    displayPicture(model);
                    model.addAttribute("user", this.userXSS);
                    return "src/xss/profile-preview";
                } else {
                    model.addAttribute("error", "Error while reading file");
                    return "src/xss/zad5";
                }
            } catch (IOException e) {
                model.addAttribute("error", "Error while reading file");
                return "src/xss/zad5";
            }
        } else {
            model.addAttribute("error", "file is required");
            return "src/xss/zad5";
        }
    }

    @GetMapping({"/xss/zad6"})
    public String getExercise6(Model model) {
        model.addAttribute("comments", comments);
        return "src/xss/zad6";
    }

    @GetMapping({"/xss/zad7"})
    public String getExercise7() {
        return "src/xss/zad7";
    }

    @PostMapping({"/xss/addComment"})
    public String addCommentXSS(Model model, @RequestParam(value = "value") String text) {
        comments.add(new Comment(text, "Leo"));
        model.addAttribute("comments", comments);
        return "src/xss/zad6";
    }

    @GetMapping({"/xss/zad8"})
    public String getExercise8() {
        return "src/xss/zad8";
    }

    @PostMapping(value = {"/xss/zad8/authorize"}, consumes = {"application/x-www-form-urlencoded"})
    public String rejectAuthorization1(@RequestParam(value = "login") String login,@RequestParam(value = "password") String password, Model model) {
        if (login.isEmpty() || password.isEmpty()) {
            model.addAttribute("error", "Credentials cannot be blank!");
        } else {
            model.addAttribute("error", "Sorry user " + XSSFilter.filterXSS1(URLDecoder.decode(login)) + " does not exists");
        }
        return "src/xss/zad8";
    }

    @GetMapping({"/xss/zad9"})
    public String getExercise9() {
        return "src/xss/zad9";
    }

    @PostMapping({"/xss/zad9/authorize"})
    public String rejectAuthorization2(@RequestParam(value = "login") String login,@RequestParam(value = "password") String password, Model model) {
        if (login.isEmpty() || password.isEmpty()) {
            model.addAttribute("error", "Credentials cannot be blank!");
        } else {
            model.addAttribute("error", "Sorry user " + XSSFilter.filterXSS2(URLDecoder.decode(login)) + " does not exists");
        }
        return "src/xss/zad9";
    }

    @GetMapping({"/xss/zad10"})
    public String getExercise10() {
        return "src/xss/zad10";
    }

    public void displayPicture(Model model) {
        if (profilePicture == null) {
            model.addAttribute("picture", "../img/blank.svg");
            model.addAttribute("isSvg", true);
        } else {
            if (extension != null && extension.equals("svg")) {
                model.addAttribute("picture",  profilePicture);
                model.addAttribute("isSvg", false);
            } else {
                model.addAttribute("picture", "data:image/" + extension + ";base64," + profilePicture);
                model.addAttribute("isSvg", true);
            }
        }
    }
}
