package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.templates.TemplatesGenerator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@Controller
public class PathTraversalController {

    @Autowired
    ResourceLoader resourceLoader;

    @GetMapping({"/path"})
    public String getMainPage() {
        return "src/path";
    }

    @GetMapping({"/test"})
    public String test() {
        return "src/pathtraversal/zad1";
    }

    @GetMapping({"/path/zad1"})
    public String getExercise1(@RequestParam(value = "cat", required = false) String src, HttpServletRequest req, HttpServletResponse response, Model model) {
        response.setHeader("X-Frame-Options", "DENY");
        src = (src == null || src.isEmpty()) ? "/static/img/cat1.webp" : URLDecoder.decode(src);
        encodeImage(src, model);
        return "src/pathtraversal/zad1";
    }

    private void encodeImage(String src, Model model) {
        if (isCat(src)) {
            try {
                byte[] bytes = new ClassPathResource(src).getInputStream().readAllBytes();
                model.addAttribute("src","data:image/jpeg;base64," + encodeFileToBase64Binary(bytes) );
            } catch (IOException e) {
                model.addAttribute("src", "error");
            }
        } else {
            try {
                File file = new File(src);
                if (file.exists()) {
                    model.addAttribute("src", "data:image/jpeg;base64," + encodeFileToBase64Binary(file));
                } else {
                    model.addAttribute("src", "none");
                }
            } catch (IOException e) {
                model.addAttribute("src", "none");
            }
        }
    }

    private String encodeImageToBase64(String src) {
        if (isCat(src)) {
            try {
                File file = new ClassPathResource(src).getFile();
                return "data:image/jpeg;base64," + encodeFileToBase64Binary(file);
            } catch (IOException e) {
                return "Not found";
            }
        } else {
            try {
                File file = new File(src);
                if (file.exists()) {
                    return "data:image/jpeg;base64," + encodeFileToBase64Binary(file);
                } else {
                    return "Not found";                }
            } catch (IOException e) {
                return "Not found";
            }
        }
    }


    public static boolean isCat(String src) {
        if (src.equals("/static/img/cat1.webp") || src.equals("/static/img/cat2.webp") || src.equals("/static/img/cat3.webp")) {
            return true;
        } else {
            return false;
        }
    }

    private static String encodeFileToBase64Binary(File file) throws IOException {
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        return new String(encoded, StandardCharsets.US_ASCII);
    }

    private static String encodeFileToBase64Binary(byte[] bytes) throws IOException {
        byte[] encoded = Base64.encodeBase64(bytes);
        return new String(encoded, StandardCharsets.US_ASCII);
    }
}
