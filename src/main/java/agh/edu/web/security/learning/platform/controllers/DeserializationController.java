package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.models.mappers.JacksonVulnerableMapper;
import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

@Controller
public class DeserializationController {

    @GetMapping({"/deserialization"})
    public String returnFrontPage(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/deserialization";
    }

    @GetMapping({"/deserialization/zad2"})
    public String getExercise2() {
        return "src/deserialization/zad2";
    }

    @PostMapping({"/deserialization/createValue"})
    @ResponseBody
    public ResponseEntity createValue(@RequestBody String json, @RequestHeader("Content-type") String contentType, HttpServletResponse response) {
        if (contentType.contains(MediaType.APPLICATION_JSON_VALUE)) {
            try {
                JacksonVulnerableMapper.parseJson(json);
                return ResponseEntity.status(201).build();
            } catch (Exception e) {
                return ResponseEntity.status(500).build();
            }
        } else {
            return ResponseEntity.status(405).build();
        }
    }

    @GetMapping({"/deserialization/zad1"})
    public String getExercise1() {
        return "src/deserialization/zad1";
    }

    @PostMapping({"/deserialization/zad1"})
    public String exploitApacheCommonsCollection(@RequestParam(value = "token") String token) {
        try {
            byte[] data = Base64.decodeBase64(token);
            ObjectInputStream obj = new ObjectInputStream(new ByteArrayInputStream(data));
            Object o = obj.readObject();
            System.out.println(o.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "src/deserialization/zad1";
    }
}
