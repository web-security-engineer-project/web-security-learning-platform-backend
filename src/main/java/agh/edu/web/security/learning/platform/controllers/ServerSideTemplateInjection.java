package agh.edu.web.security.learning.platform.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import java.io.StringWriter;

@Controller
public class ServerSideTemplateInjection {

    @RequestMapping({"/simple"})
    public String getsimple() {
        return "src/injection/ssti/simple";
    }

    @RequestMapping({"injection/ssti/simple"})
    @ResponseBody
    public String postSimpleSSTIInjection(@RequestParam(value = "value", required = false) String input) {
        return returnTemplate(input);
    }

    @RequestMapping({"injection/ssti/blind"})
    @ResponseBody
    public String postBlindSSTIInjection(@RequestParam(value = "value", required = false) String input) {
        return returnTemplate(input);
    }

    public String returnTemplate(String input) {
        String template;
        var template1 = "<!DOCTYPE html>\n" +
                "<html xmlns:th=\"https://www.thymeleaf.org\" lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Web Security Training Platform</title>\n" +
                "    <!-- CSS only -->\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/boostrap.min.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/comments.css\"/>\n" +
                "    <!-- JS, Popper.js, and jQuery -->\n" +
                "    <script src=\"../../javascript/jquery.slim.min.js\"></script>\n" +
                "    <script src=\"../../javascript/popper.min.js\"></script>\n" +
                "    <script src=\"../../javascript/boostrap.min.js\"></script>\n" +
                "    <link rel=\"stylesheet\" href=\"../../css/styles.css\"/>\n" +
                "    <script src=\"../../javascript/script.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div id=\"wrapper\" class=\"toggled\">\n" +
                "    <div class=\"overlay\"></div>\n" +
                "    <nav class=\"navbar navbar-inverse fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">\n" +
                "        <ul class=\"nav sidebar-nav\">\n" +
                "            <div class=\"sidebar-header\">\n" +
                "                <div class=\"sidebar-brand\">\n" +
                "                    <a href=\"/index\">Home</a></div>\n" +
                "            </div>\n" +
                "            <li><a href=\"/introduction\">Introduction</a></li>"+
                "            <li><a href=\"/injection\">Injection</a></li>\n" +
                "            <li><a href=\"/auth\">Broken Authorization</a></li>\n" +
                "            <li><a href=\"/xss\">Cross-site Scripting (XSS)</a></li>\n" +
                "            <li><a href=\"/xxe\">XML External Entity (XML)</a></li>\n" +
                "            <li><a href=\"/deserialization\">Deserialization</a></li>\n" +
                "            <li><a href=\"/path\">Path Traversal</a></li>\n" +
                "        </ul>\n" +
                "    </nav>\n" +
                "    <div id=\"page-content-wrapper\">\n" +
                "        <div class=\"container\">\n" +
                "            <div class=\"row\">\n" +
                "                <div class=\"col-lg-8 col-lg-offset-2\">\n" +
                "                    <h2>Please rate our product</h2>\n" +
                "                    <hr/>\n" +
                "                    <p class=\"alert alert-info\" style=\"display: none\" id=\"hint\">\n" +
                "                        The template used by page is Thymeleaf. There are at least two ways to pull out RCE in this case.\n" +
                "                    </p>"+
                "                    <div class=\"container\">\n" +
                "                        <div class=\"row justify-content-center\">\n" +
                "                            <div class=\"card w-75\">\n" +
                "                                <img class=\"card-img-top\" src=\"../../img/bmw.jpg\" alt=\"Card image cap\">\n" +
                "                                <div class=\"list-group\">\n" +
                "                                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n" +
                "                                        <div class=\"d-flex w-100 justify-content-between\">\n" +
                "                                            <h5 class=\"mb-1\">Ann Hoarde</h5>\n" +
                "                                            <small>3 days ago</small>\n" +
                "                                        </div>\n" +
                "                                        <p class=\"mb-1\">I love this motorcycle!.</p>\n" +
                "\n" +
                "                                    </div>\n" +
                "                                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n" +
                "                                        <div class=\"d-flex w-100 justify-content-between\">\n" +
                "                                            <h5 class=\"mb-1\">John Doe</h5>\n" +
                "                                            <small class=\"text-muted\">2 days ago</small>\n" +
                "                                        </div>\n" +
                "                                        <p class=\"mb-1\">It's very comfotable.</p>\n" +
                "                                    </div>\n" +
                "                                </div>\n";
        var template2 =
                "                                <div class=\"card-body\">\n" +
                "                                    <form method=\"post\" action=\"/injection/ssti/simple\">\n" +
                "                                        <div style=\"display: flex;\">\n" +
                "                                            <input style=\"flex: 1;\" class=\"form-control\" placeholder=\"type message...\" type=\"text\" name=\"value\"/>\n" +
                "                                            <button type=\"submit\" class=\"btn btn-primary ml-2\">Add comment</button>\n" +
                "                                        </div>\n" +
                "                                    </form>\n" +
                "                                </div>\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <div class=\"row justify-content-between mt-3\">\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <nav aria-label=\"Page navigation example\">\n" +
                "                                <ul class=\"pagination\">\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/injection/os/blind\" aria-label=\"Previous\">\n" +
                "                                            <span aria-hidden=\"true\">&laquo;</span>\n" +
                "                                            <span class=\"sr-only\">Previous</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad1\">1</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad2\">2</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad3\">3</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad4\">4</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad5\">5</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad6\">6</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad7\">7</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/os/simple\">8</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/os/blind\">9</a></li>\n" +
                "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/ssti/simple\">10</a></li>\n" +
                "                                    <li class=\"page-item\">\n" +
                "                                        <a class=\"page-link\" href=\"/injection/ssti/simple\" aria-label=\"Next\">\n" +
                "                                            <span aria-hidden=\"true\">&raquo;</span>\n" +
                "                                            <span class=\"sr-only\">Next</span>\n" +
                "                                        </a>\n" +
                "                                    </li>\n" +
                "                                </ul>\n" +
                "                            </nav>\n" +
                "                        </div>\n" +
                "                        <div class=\"col-auto\">\n" +
                "                            <button type=\"button\" onclick=\"showHint()\" class=\"btn btn-primary\">Show Hint</button>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        if (input != null && !input.isBlank()) {
            template = template1 + "                                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n" +
                    "                                        <div class=\"d-flex w-100 justify-content-between\">\n" +
                    "                                            <h5 class=\"mb-1\">John Doe</h5>\n" +
                    "                                            <small class=\"text-muted\">2 days ago</small>\n" +
                    "                                        </div>\n" +
                    "                                        <p class=\"mb-1\">" + input + "</p>\n" +
                    "                                    </div>" + template2;
        } else {
            template = template1 + template2;
        }
        var resolver = new StringTemplateResolver();
        resolver.setTemplateMode(TemplateMode.HTML);

        var engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);

        var out = new StringWriter();
        var context = new Context();
        try {
            engine.process(template, context, out);
        } catch (Exception e) {
            return "<!DOCTYPE html>\n" +
                    "<html xmlns:th=\"https://www.thymeleaf.org\" lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Web Security Training Platform</title>\n" +
                    "    <!-- CSS only -->\n" +
                    "    <link rel=\"stylesheet\" href=\"../../css/boostrap.min.css\">\n" +
                    "    <link rel=\"stylesheet\" href=\"../../css/comments.css\"/>\n" +
                    "    <!-- JS, Popper.js, and jQuery -->\n" +
                    "    <script src=\"../../javascript/jquery.slim.min.js\"></script>\n" +
                    "    <script src=\"../../javascript/popper.min.js\"></script>\n" +
                    "    <script src=\"../../javascript/boostrap.min.js\"></script>\n" +
                    "    <link rel=\"stylesheet\" href=\"../../css/styles.css\"/>\n" +
                    "    <script src=\"../../javascript/script.js\"></script>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\n" +
                    "<div id=\"wrapper\" class=\"toggled\">\n" +
                    "    <div class=\"overlay\"></div>\n" +
                    "    <nav class=\"navbar navbar-inverse fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">\n" +
                    "        <ul class=\"nav sidebar-nav\">\n" +
                    "            <div class=\"sidebar-header\">\n" +
                    "                <div class=\"sidebar-brand\">\n" +
                    "                    <a href=\"/index\">Home</a></div>\n" +
                    "            </div>\n" +
                    "            <li><a href=\"/introduction\">Introduction</a></li>"+
                    "            <li><a href=\"/injection\">Injection</a></li>\n" +
                    "            <li><a href=\"/auth\">Broken Authorization</a></li>\n" +
                    "            <li><a href=\"/xss\">Cross-site Scripting (XSS)</a></li>\n" +
                    "            <li><a href=\"/xxe\">XML External Entity (XML)</a></li>\n" +
                    "            <li><a href=\"/deserialization\">Deserialization</a></li>\n" +
                    "            <li><a href=\"/path\">Path Traversal</a></li>\n" +
                    "        </ul>\n" +
                    "    </nav>\n" +
                    "    <div id=\"page-content-wrapper\">\n" +
                    "        <div class=\"container\">\n" +
                    "            <div class=\"row\">\n" +
                    "                <div class=\"col-lg-8 col-lg-offset-2\">\n" +
                    "                    <h2>Please rate our product</h2>\n" +
                    "                    <hr/>\n" +
                    "                    <p class=\"alert alert-info\" style=\"display: none\" id=\"hint\">\n" +
                    "                        The template used by page is Thymeleaf. There are at least two ways to pull out RCE in this case.\n" +
                    "                    </p>" +
                    "                    <div class=\"container\">\n" +
                    "                        <div class=\"row justify-content-center\">\n" +
                    "                            <div class=\"card w-75\">\n" +
                    "                                <img class=\"card-img-top\" src=\"../../img/bmw.jpg\" alt=\"Card image cap\">\n" +
                    "                                <div class=\"list-group\">\n" +
                    "                                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n" +
                    "                                        <div class=\"d-flex w-100 justify-content-between\">\n" +
                    "                                            <h5 class=\"mb-1\">Ann Hoarde</h5>\n" +
                    "                                            <small>3 days ago</small>\n" +
                    "                                        </div>\n" +
                    "                                        <p class=\"mb-1\">I love this motorcycle!.</p>\n" +
                    "\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"list-group-item list-group-item-action flex-column align-items-start\">\n" +
                    "                                        <div class=\"d-flex w-100 justify-content-between\">\n" +
                    "                                            <h5 class=\"mb-1\">John Doe</h5>\n" +
                    "                                            <small class=\"text-muted\">2 days ago</small>\n" +
                    "                                        </div>\n" +
                    "                                        <p class=\"mb-1\">It's very comfotable.</p>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"card-body\">\n" +
                    "                                    <form method=\"post\" action=\"/injection/ssti/simple\">\n" +
                    "                                        <div style=\"display: flex;\">\n" +
                    "                                            <input style=\"flex: 1;\" class=\"form-control\" placeholder=\"type message...\" type=\"text\" name=\"value\"/>\n" +
                    "                                            <button type=\"submit\" class=\"btn btn-primary ml-2\">Add comment</button>\n" +
                    "                                        </div>\n" +
                    "                                    </form>\n" +
                    "                                </div>\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                    <div class=\"row justify-content-between mt-3\">\n" +
                    "                        <div class=\"col-auto\">\n" +
                    "                            <nav aria-label=\"Page navigation example\">\n" +
                    "                                <ul class=\"pagination\">\n" +
                    "                                    <li class=\"page-item\">\n" +
                    "                                        <a class=\"page-link\" href=\"/injection/os/blind\" aria-label=\"Previous\">\n" +
                    "                                            <span aria-hidden=\"true\">&laquo;</span>\n" +
                    "                                            <span class=\"sr-only\">Previous</span>\n" +
                    "                                        </a>\n" +
                    "                                    </li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad1\">1</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad2\">2</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad3\">3</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad4\">4</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad5\">5</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad6\">6</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/zad7\">7</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/os/simple\">8</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/os/blind\">9</a></li>\n" +
                    "                                    <li class=\"page-item\"><a class=\"page-link\" href=\"/injection/ssti/simple\">10</a></li>\n" +
                    "                                    <li class=\"page-item\">\n" +
                    "                                        <a class=\"page-link\" href=\"/injection/ssti/simple\" aria-label=\"Next\">\n" +
                    "                                            <span aria-hidden=\"true\">&raquo;</span>\n" +
                    "                                            <span class=\"sr-only\">Next</span>\n" +
                    "                                        </a>\n" +
                    "                                    </li>\n" +
                    "                                </ul>\n" +
                    "                            </nav>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"col-auto\">\n" +
                    "                            <button type=\"button\" onclick=\"showHint()\" class=\"btn btn-primary\">Show Hint</button>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </div>\n" +
                    "    </div>\n" +
                    "</div>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>";
        }
        return out.toString();
    }
}
