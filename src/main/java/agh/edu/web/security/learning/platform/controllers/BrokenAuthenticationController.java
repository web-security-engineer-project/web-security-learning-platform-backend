package agh.edu.web.security.learning.platform.controllers;

import agh.edu.web.security.learning.platform.models.Product;
import agh.edu.web.security.learning.platform.models.User;
import agh.edu.web.security.learning.platform.repositories.interfaces.ProductRepository;
import agh.edu.web.security.learning.platform.repositories.interfaces.UserRepository;
import agh.edu.web.security.learning.platform.utils.HttpHeaderUtil;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping({"/auth"})
public class BrokenAuthenticationController {

    @Autowired
    public UserRepository userRepository;
    @Autowired
    public ProductRepository productRepository;

    public BrokenAuthenticationController(UserRepository userRepository, ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    @RequestMapping({""})
    public String injection(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth";
    }

    @RequestMapping({"/zad1"})
    public String getExercise1(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad1";
    }

    @RequestMapping({"/zad2"})
    public String getExercise2(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad2";
    }

    @RequestMapping({"/zad3"})
    public String getExercise3(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad3";
    }

    @RequestMapping({"/zad4"})
    public String getExercise4(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad4";
    }

    @RequestMapping({"/zad5"})
    public String getExercise5(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad5";
    }

    @RequestMapping({"/zad6"})
    public String getExercise6(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad6";
    }

    @RequestMapping({"/zad7"})
    public String getExercise7(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "src/auth/zad7";
    }

    @GetMapping({"/zad8"})
    public String getExercise8(HttpServletResponse response, Model model) {
        HttpHeaderUtil.addHeaders(response);
        Optional<User> user = userRepository.findUserById("1");
        if (user.isPresent()) {
            model.addAttribute("user", user.get());
            return "src/auth/zad8";
        } else {
            response.setStatus(404);
            return "src/other/error";
        }
    }

    @GetMapping({"/idor/{id}"})
    public String getDetailedInfo(@PathVariable String id, HttpServletResponse response, Model model) {
        Optional<User> user = userRepository.findUserById(id);
        if (user.isPresent()) {
            model.addAttribute("user", user.get());
            List<Product> productList = productRepository.getAllUserProducts(id);
            model.addAttribute("products", productList);
            return "src/auth/result/user-info";
        } else {
            response.setStatus(404);
            return "src/other/error";
        }

    }

    @GetMapping({"/password-recovery-email"})
    public String getPasswordRecoveryPage() {
        return "src/auth/password-recovery-email";
    }

    @GetMapping({"/zad2/credentials.txt"})
    @ResponseBody
    public String returnCredentials(HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        return "login=admin&" +
                "password=ht4CA\\jY]A{k\"j]D";
    }

    @PostMapping({"/zad1/authorize"})
    public ModelAndView processCredentialsExercise1(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password, HttpServletResponse response) {
        var modelAndView = new ModelAndView();
        HttpHeaderUtil.addHeaders(response);
        var loginExercise = "dev";
        var passwordExercise = "n^4RSp(N5UdTzA5X";
        if (!login.equals(loginExercise) || !passwordExercise.equals(password)) {
            modelAndView.addObject("forbidden", "Sorry, your credentials are invalid.");
            response.setStatus(401);
            modelAndView.setViewName("src/auth/zad1");
            return modelAndView;
        } else {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 1!");
            modelAndView.addObject("linkToExercise", "/auth/zad2");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        }
    }

    @PostMapping({"/zad2/authorize"})
    public ModelAndView processCredentialsExercise2(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password, HttpServletResponse response) {
        var modelAndView = new ModelAndView();
        HttpHeaderUtil.addHeaders(response);
        var loginExercise = "admin";
        var passwordExercise = "ht4CA\\jY]A{k\"j]D";
        if (!login.equals(loginExercise) || !passwordExercise.equals(password)) {
            modelAndView.addObject("forbidden", "Sorry, your credentials are invalid");
            response.setStatus(401);
            modelAndView.setViewName("src/auth/zad2");
            return modelAndView;
        } else {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 2!");
            modelAndView.addObject("linkToExercise", "/auth/zad3");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        }
    }

    @PostMapping({"/zad3/authorize"})
    public ModelAndView processCredentialsExercise3(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password, HttpServletResponse response) {
        var modelAndView = new ModelAndView();
        HttpHeaderUtil.addHeaders(response);
        var user = new User();
        user.setEmail(login);
        user.setPassword(password);
        var authorized = userRepository.userAuthorization(user);
        if (!authorized) {
            modelAndView.addObject("forbidden", "Sorry, your credentials are invalid");
            response.setStatus(401);
            modelAndView.setViewName("src/auth/zad3");
            return modelAndView;
        } else {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 3!");
            modelAndView.addObject("linkToExercise", "/auth/zad4");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        }
    }

    @PostMapping({"/zad4/authorize"})
    public ModelAndView processCredentialsExercise4(@RequestParam(value = "login", required = false) String login, @RequestParam(value = "password", required = false) String password, HttpServletResponse response) {
        var modelAndView = new ModelAndView();
        HttpHeaderUtil.addHeaders(response);
        if (!login.isEmpty() || !password.isEmpty()) {
            modelAndView.addObject("forbidden", "Sorry, your credentials are invalid");
            response.setStatus(401);
            modelAndView.setViewName("src/auth/zad4");
            return modelAndView;
        } else {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 4!");
            modelAndView.addObject("linkToExercise", "/auth/zad5");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        }
    }

    @PostMapping({"/zad5/authorize"})
    public ModelAndView processCredentialsExercise5(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password, HttpServletResponse response ) {
        var modelAndView = new ModelAndView();
        HttpHeaderUtil.addHeaders(response);
        String loginBruteForce = "cat";
        String passwordBruteForce = "cat";
        if (!login.equals(loginBruteForce) || !password.equals(passwordBruteForce)) {
            modelAndView.setViewName("src/auth/zad5");
            modelAndView.addObject("forbidden", "Sorry, your credentials are invalid");
            response.setStatus(401);
            return modelAndView;
        } else {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 5!");
            modelAndView.addObject("linkToExercise", "/auth/zad6");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        }
    }

    @PostMapping({"/zad6/authorize"})
    public ModelAndView processCredentialsExercise6(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password, HttpServletResponse response) {
        var modelAndView = new ModelAndView();
        HttpHeaderUtil.addHeaders(response);
        String loginExercise = "mark@mail.com";
        String passwordExercise = "@9XCDFy.TG6ceC<n";
        if (!login.equals(loginExercise) || !password.equals(passwordExercise)) {
            modelAndView.setViewName("src/auth/zad6");
            modelAndView.addObject("forbidden", "Sorry, your credentials are invalid");
            response.setStatus(401);
            return modelAndView;
        } else {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 6!");
            modelAndView.addObject("linkToExercise", "/auth/zad7");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        }
    }

    @PostMapping({"/zad6/password-question"})
    public ModelAndView processToPasswordRecoveryPage(@RequestParam(value = "login") String login, HttpServletResponse response) {
        var modelAndView = new ModelAndView();
        var loginRequest = "mark@mail.com";
        if (login.equals(loginRequest)) {
            response.addCookie(new Cookie("security-cookie", "Nfa`$x4aE3k7qqS#>!w8Z>`[<"));
            modelAndView.setViewName("src/auth/password-recovery");
            return modelAndView;
        } else {
            modelAndView.setViewName("src/auth/password-recovery-email");
            modelAndView.addObject("notfound", "User does not exists!");
            response.setStatus(404);
            return modelAndView;
        }
    }

    @PostMapping({"/zad6/password-recovery-question"})
    public ModelAndView returnUserPassword(@CookieValue(value = "security-cookie") String cookie, @RequestParam(value = "color") String answer, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView();
        if (!cookie.equals("Nfa`$x4aE3k7qqS#>!w8Z>`[<")) {
            response.setStatus(401);
            modelAndView.setViewName("src/auth/zad6");
            return modelAndView;
        } else {
            if (answer.equals("White")) {
                modelAndView.addObject("password", "@9XCDFy.TG6ceC<n");
                modelAndView.setViewName("src/auth/password-recovery");
                return modelAndView;
            } else {
                modelAndView.addObject("error", "Wrong answer!");
                modelAndView.setViewName("src/auth/password-recovery");
                return  modelAndView;
            }
        }
    }

    @PostMapping({"/zad7/authorize"})
    public ModelAndView requestUserPassword(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password, HttpServletResponse response) {
        HttpHeaderUtil.addHeaders(response);
        var modelAndView = new ModelAndView();
        var loginExercise = "youshould";
        var passwordExercise = "notusebasicauth";
        if (loginExercise.equals(login) && passwordExercise.equals(password)) {
            modelAndView.addObject("success", "Congratulations, you have passed successfully exercise 7!");
            modelAndView.addObject("linkToExercise", "/jwt/zad1");
            modelAndView.setViewName("src/auth/result/success");
            return modelAndView;
        } else {
            modelAndView.setViewName("src/auth/zad7");
            modelAndView.addObject("forbidden", "Wrong credentials");
            response.setStatus(401);
            return modelAndView;
        }
    }
}
