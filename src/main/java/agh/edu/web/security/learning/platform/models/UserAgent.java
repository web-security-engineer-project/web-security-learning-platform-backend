package agh.edu.web.security.learning.platform.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_agent")
public class UserAgent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(nullable = false, name = "user_id")
    private int userId;
    @Column(name = "user_agent")
    private String userAgentName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAgent userAgent = (UserAgent) o;
        return id == userAgent.id &&
                userId == userAgent.userId &&
                Objects.equals(userAgentName, userAgent.userAgentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, userAgentName);
    }

    public UserAgent(int id, int userId, String userAgentName) {
        this.id = id;
        this.userId = userId;
        this.userAgentName = userAgentName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserAgent() {
    }

    public UserAgent(int userId, String userAgentName) {
        this.userId = userId;
        this.userAgentName = userAgentName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserAgentName() {
        return userAgentName;
    }

    public void setUserAgentName(String userAgentName) {
        this.userAgentName = userAgentName;
    }
}
