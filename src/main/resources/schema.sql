DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS product_transaction;

CREATE TABLE users(
    id int not null primary key auto_increment,
    name VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255)  NOT NULL,
    personal_information VARCHAR(255)
    );

CREATE TABLE products(
    id int not null primary key auto_increment,
    name VARCHAR(30) NOT NULL,
    price VARCHAR(20) NOT NULL,
    description VARCHAR(255),
    type varchar(10) NOT NULL
    );

CREATE TABLE user_agent(
    id int not null primary key auto_increment,
    user_id int not null,
    user_agent VARCHAR(255) not null
);

CREATE TABLE product_transaction(
    id int not null primary key auto_increment,
    product_id int not null,
    user_id int not null
);