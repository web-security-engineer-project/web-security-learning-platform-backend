const xml = () => {
    const text = document.getElementById('comment');
    return`<?xml version='1.0' encoding='UTF-8'?>
        <comment>
        <user>guest</user>
           <text>${text.value}</text>
      </comment>         
    `;
}

function addComment() {
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = () => {
        if (xmlHttpRequest.response == 1) {
            location.reload();
        } else if (xmlHttpRequest.response == -1 || xmlHttpRequest.response == -2) {
            location.reload();
        }
    }
    xmlHttpRequest.open('POST', 'http://localhost:8080/xxe/addComment', true);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/xml");
    xmlHttpRequest.send(unescape(xml()));
}

function removeComment(id) {
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = () => {
        if (xmlHttpRequest.readyState == 4) {
            if (xmlHttpRequest.status == 404 || xmlHttpRequest.status == 401) {
                document.getElementById('error').style.visibility = 'visible';
            } else if(xmlHttpRequest.status == 204) {
                location.reload();
            }
        }
    }

    xmlHttpRequest.open("DELETE", `http://localhost:8080/jwt/zad1/deleteComment/${id + 1}`, true);
    xmlHttpRequest.setRequestHeader("token", getCookie('token'))
    xmlHttpRequest.send("whatever");
}

function removeComment2(id) {
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = () => {
        if (xmlHttpRequest.readyState == 4) {
            if (xmlHttpRequest.status == 404 || xmlHttpRequest.status == 401) {
                document.getElementById('error').style.visibility = 'visible';
                console.log(xmlHttpRequest.response);
            } else if(xmlHttpRequest.status == 204) {
                location.reload();
            }
        }
    }

    xmlHttpRequest.open("DELETE", `http://localhost:8080/jwt/zad2/deleteComment/${id + 1}`, true);
    xmlHttpRequest.setRequestHeader("token", getCookie('token'))
    xmlHttpRequest.send("whatever");
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}