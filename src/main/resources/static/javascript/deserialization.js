function createJson() {
    return `{
                "vulnerableField" : "${document.getElementById('vulnerableField').value}",
                "value": "${document.getElementById('valueField').value}"
            }`;
}

function createEntity() {
    let httpRequest = new XMLHttpRequest();
    httpRequest.open("POST", "http://localhost:8080/deserialization/createValue");
    httpRequest.onreadystatechange = () => {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 405 || httpRequest.status == 500) {
                document.getElementById('response').textContent = "Unsuccessfully created resource";
            } else if(httpRequest.status == 201) {
                document.getElementById('response').textContent = "Successfully created resource";
            }
        }
    }
    httpRequest.setRequestHeader("Content-type", "application/json");
    httpRequest.send(createJson());
}