const xml = () => {
    const name = document.getElementById('name');
    const price = document.getElementById('price');
    const description = document.getElementById('description');
    const type = document.getElementById('type');
    return`<?xml version='1.0' encoding='UTF-8'?>
      <product>
        <id>1</id>
        <name>${name.value}</name>
        <price>${price.value}</price>
        <description>${description.value}</description>
        <type>${type.value}</type>
      </product>         
    `;
}
function addProduct() {
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = () => {
        if (xmlHttpRequest.response == 1) {
            document.getElementById('success').style.visibility = 'visible';
            document.getElementById('failure').style.visibility = 'hidden';
        } else if (xmlHttpRequest.response == -1 || xmlHttpRequest.response == -2) {
            document.getElementById('success').style.visibility = 'hidden';
            document.getElementById('failure').style.visibility = 'visible';
        }
    }
    xmlHttpRequest.open('POST', 'http://localhost:8080/xxe/addProduct', false);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/xml");
    xmlHttpRequest.send(unescape(xml()));
}
