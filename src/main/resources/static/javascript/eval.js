const HEX = {
    red: '#FF0000',
    yellow: '#FFFF00',
    blue: '#0000FF',
    black: '#000000',
    white: '#FFFFFF',
    green: '#008000',
    purple: '#800080',
    orange: '#FFA500',
    pink: '#FFC0CB',
    brown: '#A52A2A',
    gold: '#FFD700',
    khaki: '#F0E68C',
    violet: '#EE82EE',
    magenta: '#FF20FF',
    indigo: '#4B0082',
    grey: '#808080',
    gray: '#808080',
    silver: '#C0C0C0',
    lavender: '#E6E6FA',
    lime: '#00FF00',
    olive: '#808000',
    coral: '#FF7F50',
    tomato: '#FF6437',
    salmon: '#FA8072'
};


function evaluateExpression() {
    document.getElementById('output').textContent = eval('HEX.' + document.getElementById('evalinput').value);
}