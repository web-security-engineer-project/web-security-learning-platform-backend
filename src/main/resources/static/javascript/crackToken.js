function crackToken() {
    const token = document.getElementById('tokenInput')
    const messageError =  document.getElementById('displayMessageError')
    const messageSuccess =  document.getElementById('displayMessageSuccess')
    const httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = () => {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                messageSuccess.style.display = 'block'
                messageError.style.display = 'none'
            } else {
                messageError.style.display = 'block'
                messageSuccess.style.display = 'none'
            }
        }
    }
    httpRequest.open('POST', 'http://localhost:8080/jwt/zad3/crack')
    httpRequest.send(token.value);
}