INSERT INTO users(name, password, email, personal_information) VALUES('admin', 'hardpassword', 'some@email.pl', 'root user information');
INSERT INTO users(name, password, email, personal_information) VALUES('someuser', '12345678', 'some@user.email.pl', 'my important personal info');
INSERT INTO users(name, password, email, personal_information) VALUES('securityisreal', 'itsnotagoodpassword', 'secure@email.pl', 'secured information');

INSERT INTO user_agent(user_id, user_agent) VALUES(1, '');
INSERT INTO user_agent(user_id, user_agent) VALUES(2, '');
INSERT INTO user_agent(user_id, user_agent) VALUES(3,  '');

INSERT INTO products(name, price, description, type) VALUES('knight tom', '19.22', 'legend about knight tom', 'BOOK');
INSERT INTO products(name, price, description, type) VALUES('tales of dunder', '99.99', 'Story about dunder mifflin', 'BOOK');
INSERT INTO products(name, price, description, type) VALUES('cake', '3.33', 'with strawberry flavour', 'FOOD');
INSERT INTO products(name, price, description, type) VALUES('pie', '2.64', 'stuffed with apple', 'FOOD');
INSERT INTO products(name, price, description, type) VALUES('gummy bear', '4.58', '78% sugar', 'FOOD');
INSERT INTO products(name, price, description, type) VALUES('pancake', '2.25', 'pancake with clone syrup', 'FOOD');
INSERT INTO products(name, price, description, type) VALUES('calculator', '24.22', 'very precise calculator', 'ELECTRONIC');
INSERT INTO products(name, price, description, type) VALUES('watch', '24.22', 'precious watch', 'ELECTRONIC');


INSERT INTO product_transaction(product_id, user_id) VALUES(1, 1);
INSERT INTO product_transaction(product_id, user_id) VALUES(5, 1);
INSERT INTO product_transaction(product_id, user_id) VALUES(3, 1);
INSERT INTO product_transaction(product_id, user_id) VALUES(4, 2);
INSERT INTO product_transaction(product_id, user_id) VALUES(5, 2);
INSERT INTO product_transaction(product_id, user_id) VALUES(2, 3);
INSERT INTO product_transaction(product_id, user_id) VALUES(6, 1);
INSERT INTO product_transaction(product_id, user_id) VALUES(7, 1);
INSERT INTO product_transaction(product_id, user_id) VALUES(8, 2);
INSERT INTO product_transaction(product_id, user_id) VALUES(8, 3);
